module.exports = {
  setupFilesAfterEnv: [
    '<rootDir>/src/testUtils/setup.js',
  ],
  testMatch: [
    '<rootDir>/src/__tests__/*.js',
    '<rootDir>/src/**/*.spec.js',
  ],
  testPathIgnorePatterns: [
    '/node_modules/',
    '<rootDir>/dist/',
  ],
};
