<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=# AutoCRUD _(${name})_) -->
# AutoCRUD _(@vizworx/autocrud)_
<!-- AUTO-GENERATED-CONTENT:END -->

[![NPM Version][package-version-svg]][package-url]
![](https://gitlab.com/vizworx/public/autocrud/badges/master/pipeline.svg)
[![License][license-image]][license-url]

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=${description}&unknownTxt=) -->
Tools for generating resolvers and Knex queries from a GraphQL schema
<!-- AUTO-GENERATED-CONTENT:END -->

AutoCRUD is intended to simplify the process of connecting a GraphQL API and database - in many cases eliminating the need to write resolvers at all.

<!-- AUTO-GENERATED-CONTENT:START (TOC) -->
- [Documentation](#documentation)
- [Usage](#usage)
  - [Minimal example](#minimal-example)
- [Assumptions](#assumptions)
- [Limitations](#limitations)
- [Development](#development)
  - [Testing](#testing)
  - [Running with local projects](#running-with-local-projects)
    - [Yarn-based projects](#yarn-based-projects)
    - [NPM-based projects](#npm-based-projects)
- [License](#license)
<!-- AUTO-GENERATED-CONTENT:END -->

## Documentation
- [Server setup](https://gitlab.com/vizworx/public/autocrud/-/blob/develop/docs/server-setup.md)
- [Directives](https://gitlab.com/vizworx/public/autocrud/-/blob/develop/docs/directives.md)
- [Hooks](https://gitlab.com/vizworx/public/autocrud/-/blob/develop/docs/hooks.md)
- [Event reporting](https://gitlab.com/vizworx/public/autocrud/-/blob/develop/docs/event-reporting.md)

## Usage

<!-- AUTO-GENERATED-CONTENT:START (INSTALLCMD:optional=true) -->
```sh
yarn add @vizworx/autocrud "graphql@^14.0.0 || ^15.0.0" "knex@^2.0 || ^1.0 || ^0.95"
```
<!-- AUTO-GENERATED-CONTENT:END -->

### Minimal example

This example uses `apollo-server-express`, but Autocrud should be expected to work with any library following the GraphQL spec.

```js
import { ApolloServer, makeExecutableSchema, gql } from 'apollo-server-express';
import {
  initialize as initializeAutoCRUD,
  typeDefs as AutoCRUDTypeDefs,
} from '@vizworx/autocrud';

import knex from './setupKnexInstance';
import typeDefs from './typeDefs';

const schema = makeExecutableSchema({
  typeDefs: [
    ...AutoCRUDTypeDefs,
    gql`
      type User @autocrud(table: "users") {
        id: ID!
        createdAt: DateTime!
        updatedAt: DateTime!
        email: String!
        name: String!
        avatarURL: String!
      }
    `,
  ],
});

const { getContext: getAutoCRUDContext } = initializeAutoCRUD(schema);

return new ApolloServer({
  schema,
  context: () => ({
    ...getAutoCRUDContext({ knex }),
  }),
});
```

## Assumptions
- All database tables referenced from an `@autocrud(table: ___)` declaration must have an `id` column. This does _not_ apply to pivot relationships, as `{ leftId, rightId }` are sufficient, unless the schema exposes that table directly:
  ```
  type AllRelationships @autocrud(table: "relationships") {
    leftId
    rightId
  }
  ```

## Limitations
- MySQL and PostgreSQL are the only _officially_ supported databases at this time. Our tests are also run against SQLite (minimum version of 3.30.0), and it is "unsupported" only because its flexibility means we can't guarantee everything will behave correctly. Contributions providing support for other databases are more than welcome.
- Microsoft SQL Server is explicitly _not_ supported; `node-mssql` has dropped support for running database transactions in a `Promise.all` (https://github.com/tediousjs/node-mssql/issues/491).
- [!11](https://gitlab.com/vizworx/public/autocrud/-/issues/11) - AutoCRUD does not currently support union types. Fields with a union type will be omitted when generating `Filter` and `OrderBy` inputs, and queries with union fields will probably throw an error.
- Interface fields do not currently support the `through` argument for relationships.

## Development

### Testing
- `yarn test`: Run the test suite using an in-memory SQLite instance. This is faster than using the Docker scripts but also prone to error, as SQLite has fewer constraints than other database clients.
- `yarn test:start-mysql`
- `yarn test:start-postgres`: Start a database using Docker. (Will block the current terminal until it is stopped)
- `yarn test:mysql`
- `yarn test:postgres`: Run the test suite.
- `yarn test:stop-mysql`
- `yarn test:stop-postgres`: Stop the Docker container.

### Running with local projects
Because AutoCRUD relies on peer dependencies, linking it to a local project requires a few extra steps.

When developing locally `yarn` will install everything, allowing AutoCRUD's tests, linting, etc. to be run.

Peer dependencies need to be removed when using AutoCRUD from another package, however, so we've added a script to take care of building, installing, and linking the project for you.

#### Yarn-based projects
```bash
# /autocrud
yarn run deploy-local

# /other-project
yarn link ../path/to/autocrud
yarn run start --preserve-symlinks
```

#### NPM-based projects
```bash
# /autocrud
npm run deploy-local

# /other-project
npm link @vizworx/autocrud
npm run start -- --preserve-symlinks
```

In either case, depending on your project's configuration you may have to experiment with adding/removing the extra ` -- ` when starting to have `--preserve-symlinks` passed correctly to the runtime.

## License
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[${license}][license-url] © VizworX) -->
[MIT][license-url] © VizworX
<!-- AUTO-GENERATED-CONTENT:END -->

<!-- VARIABLES -->

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-version-svg]: https://img.shields.io/npm/v/${name}.svg?style=flat-square) -->
[package-version-svg]: https://img.shields.io/npm/v/@vizworx/autocrud.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-url]: https://www.npmjs.com/package/${name}) -->
[package-url]: https://www.npmjs.com/package/@vizworx/autocrud
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[license-image]: https://img.shields.io/badge/license-${license}-green.svg?style=flat-square) -->
[license-image]: https://img.shields.io/badge/license-MIT-green.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
[license-url]: ./LICENSE
