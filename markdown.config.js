/* eslint-disable import/no-extraneous-dependencies */
const PKGJSON = require('markdown-magic-package-json');
const INSTALLCMD = require('markdown-magic-install-command');

module.exports = {
  transforms: {
    PKGJSON,
    INSTALLCMD,
  },
};
