import serializer from 'jest-serializer-ansi';

expect.addSnapshotSerializer(serializer);

describe('setup', () => {
  describe('expect.extend', () => {
    describe('toEqualComparator', () => {
      describe('array', () => {
        test('valid', () => {
          expect(() => {
            expect({ test: ['a', 'b'] }).toEqualComparator(
              { test: ['b', 'a'] },
              { array: (received, expected) => expect(received).toContainExactly(expected) },
            );
          }).not.toThrow();
        });

        test('invalid', () => {
          expect(() => {
            expect({ test: ['a', 'b'] }).toEqualComparator(
              { test: ['c', 'a'] },
              { array: (received, expected) => expect(received).toContainExactly(expected) },
            );
          }).toThrowErrorMatchingSnapshot();
        });
      });

      describe('object', () => {
        test('valid', () => {
          expect(() => {
            expect({ test: true }).toEqualComparator(
              { test: true },
              { object: (received, expected) => expect(received).toEqual(expected) },
            );
          }).not.toThrow();
        });

        test('invalid', () => {
          expect(() => {
            expect({ test: true }).toEqualComparator(
              { test: false },
              { object: (received, expected) => expect(received).toEqual(expected) },
            );
          }).toThrowErrorMatchingSnapshot();
        });
      });

      describe('string', () => {
        test('valid', () => {
          expect(() => {
            expect('a').toEqualComparator(
              'a',
              {
                string: (received, expected) => expect(received.toUpperCase())
                  .toEqual(expected.toUpperCase()),
              },
            );
          }).not.toThrow();
        });

        test('invalid', () => {
          expect(() => {
            expect('a').toEqualComparator(
              'b',
              {
                string: (received, expected) => expect(received.toUpperCase())
                  .toEqual(expected.toUpperCase()),
              },
            );
          }).toThrowErrorMatchingSnapshot();
        });
      });

      describe('number', () => {
        test('valid', () => {
          // These are unrealistic operations just to test that the custom
          // comparator works for this type
          expect(() => {
            expect(1).toEqualComparator(
              2,
              { number: (received, expected) => expect(received * 2).toEqual(expected) },
            );
          }).not.toThrow();
        });

        test('invalid', () => {
          expect(() => {
            expect(1).toEqualComparator(
              4,
              { number: (received, expected) => expect(received * 2).toEqual(expected) },
            );
          }).toThrowErrorMatchingSnapshot();
        });
      });

      describe('Date', () => {
        const testDate = new Date('December 25, 2000 00:00:00 UTC');
        const expectDate = new Date('December 25, 2000 12:00:00 UTC');

        test('valid', () => {
          expect(() => {
            expect(testDate).toEqualComparator(
              expectDate,
              {
                Date: (received, expected) => {
                  const convertDate = new Date(received.getTime() + 12 * 60 * 60 * 1000);
                  expect(convertDate).toEqual(expected);
                },
              },
            );
          }).not.toThrow();
        });

        test('invalid', () => {
          expect(() => {
            expect(testDate).toEqualComparator(
              expectDate,
              { Date: (received) => expect(received).toEqual(new Date('January 1, 2000 00:00:00 UTC')) },
            );
          }).toThrowErrorMatchingSnapshot();
        });
      });
    });
  });
});
