/* eslint no-underscore-dangle: ["error", { "allow": ["_autocrudInfo"] }] */
import { graphql } from 'graphql';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { v4 as uuidv4 } from 'uuid';

import getKnexInstance from './knex';
import createRequestDataLoaders from '../createRequestDataLoaders';
import { applyStagedDirectives } from '../utilities';
import getHookResolver from '../hooks';

/**
 * Provides a Knex instance and GraphQL functionality for easily testing
 * GraphQL-based code, such as custom directives. Knex can be mocked or a real DB.
 *
 * #### GraphQL options
 * `directives` - A set of staged directives. If a directive doesn't need to
 * be staged, just wrap it in an array and it will be applied in the first stage.
 * ```
 *   directives: {
 *     apples: [AppleDirective]
 *     oranges: [OrangeDirectiveStage1, OrangeDirectiveStage2],
 *   }
 * ```
 *
 * #### Tester options
 *
 * `mockDB` - False to use a real database (SQLite or MySQL if environment args
 *   are provided), or an object to use a mocked database. The object should match:
 * * `mockDbResponses` - An array of responses which will be returned in sequence
 *     as database requests come in. Each response can be an array, which will be
 *     returned directly, or a callback which will be passed all of the current
 *     query information. Use this, for example, if the code under test has
 *     generated any data dynamically that the database will need to return later.
 *
 *     i.e.
 *
 * ```
    let newId;
    tester.mockDbResponses = [
      // "Inserting" a record with a dynamic ID
      ({ bindings: [id, name] }) => {
        newId = id;

        return [{ id, name }];
      },
      ...other queries...
      // Selecting the ID later
      () => [{ id: newId }],
    ];
 * ```
 *
 * * `printQueries` - Requests and their responses will be logged out to the console.
 *
 * * `handleTransactions` - Transactions requests (begin, commit, and rollback)
 *     will have an empty array returned without affecting the sequence of mock
 *     responses.
 */

const testersToDestroy = [];

class AutoCRUDTester {
  #destroyKnex = () => {};
  #loadingKnex = null;

  constructor(
    {
      typeDefs,
      resolvers,
      directives,
      hooks = null,
      pubsub = null,
      pubsubMetadata = null,
      options = {},
      context = {},
    },
  ) {
    this.uuids = {};
    this.#loadingKnex = getKnexInstance();

    this.schema = makeExecutableSchema({
      typeDefs,
      resolvers,
    });

    this.schema._autocrudInfo ||= {};
    this.schema._autocrudInfo.options = options;

    if (directives) { applyStagedDirectives(this.schema, directives); }

    const { AutoCRUD: autocrudContext, ...otherContext } = context;
    this.getContext = () => ({
      ...otherContext,
      tester: this,
      AutoCRUD: {
        knex: this.knex,
        pubsub,
        pubsubMetadata,
        resolveHook: getHookResolver(hooks),
        ...createRequestDataLoaders(this.knex, this.schema),
        ...autocrudContext,
      },
    });

    testersToDestroy.push(this);

    return this.#init();
  }

  #init = async () => {
    const { knex, destroyKnex, resetDB } = await this.#loadingKnex;
    this.knex = knex;
    this.#destroyKnex = destroyKnex;
    this.resetDB = resetDB;

    return this;
  }

  async query(query, variables, { AutoCRUD: autocrudContext, ...context } = {}) {
    const { AutoCRUD: parentAutocrudContext, ...parentContext } = this.getContext();

    const AutoCRUD = {
      ...parentAutocrudContext,
      ...(autocrudContext || {}),
    };

    return graphql(
      this.schema,
      query,
      undefined,
      {
        ...parentContext,
        ...(context || {}),
        AutoCRUD,
      },
      variables,
    );
  }

  // This will generate a consistent UUID for the `name` for this tester
  uuid(name) {
    if (!this.uuids[name]) { this.uuids[name] = uuidv4(); }

    return this.uuids[name];
  }

  async destroy() {
    if (this.#destroyKnex) { await this.#destroyKnex(); }
  }
}

export const destroyTesters = () => Promise
  .all(testersToDestroy.map((tester) => tester.destroy()));

export default AutoCRUDTester;
