import cloneDeep from 'clone-deep';
import { destroyTesters } from './AutoCRUDTester';
import { destroyKnexClients } from './knex';
import './expect-graphql';

afterAll(async () => {
  await destroyTesters();
  await destroyKnexClients();
});

const stringifyReplacer = (k, v) => {
  if (typeof v === 'function') {
    return v.toString();
  }

  return v;
};

expect.extend({
  /**
   * Expects that the given values occur at some point in the received array,
   * in the order given, but not necessarily together. i.e.
   *
   * ```
   * const arr = [
   *   1,
   *   'apple',
   *   2,
   *   'banana',
   *   3,
   *   'cherry',
   * ]
   *
   * // Passes
   * expect(arr).toContainInOrder([
   *   1,
   *   2,
   *   (v) => v.match('che'),
   * ]);
   * ```
   *
   * @param {Array} received
   * @param {Array} expected An array of values or `find` callbacks
   */
  toContainInOrder(received, expected) {
    const found = [];
    let startIndex = 0;
    let foundAll = true;

    for (let i = 0; i < expected.length; i += 1) {
      const f = expected[i];

      const findIn = received.slice(startIndex);
      const foundIndex = findIn[(typeof f) === 'function' ? 'findIndex' : 'indexOf'](f);

      if (foundIndex > -1) {
        const realIndex = foundIndex + startIndex;
        found.push([realIndex, received[realIndex]]);

        startIndex = foundIndex + 1;
      } else {
        foundAll = false;
        break;
      }
    }

    return (foundAll)
      ? {
        message: () => `expected array not to contain the given items in order\n\nNot Expected:\n${JSON.stringify(expected, stringifyReplacer, 2)}\n\nGot:\n${JSON.stringify(received, null, 2)}`,
        pass: true,
      }
      : {
        message: () => `expected array to contain the given items in order\n\nExpected:\n${JSON.stringify(expected, stringifyReplacer, 2)}\n\nGot:\n${JSON.stringify(received, null, 2)}`,
        pass: false,
      };
  },

  array(received) {
    return (received instanceof Array || Array.isArray(received))
      ? {
        message: () => `expected not to be an array, got:\n${received}`,
        pass: true,
      }
      : {
        message: () => `expected to be an array, got:\n${received}`,
        pass: false,
      };
  },

  /**
   * Expects arrays to have the same contents and length, irrespective of order.
   * @param {Array} received
   * @param {Array} expected
   */
  toContainExactly(received, expected) {
    if (this.isNot) {
      expect(received).not.toEqual(expect.arrayContaining(expected));
      expect(received.length).not.toEqual(expected.length);
    } else {
      expect(received).toEqual(expect.arrayContaining(expected));
      expect(received.length).toEqual(expected.length);
    }

    return { pass: !this.isNot };
  },

  toEqualComparator(receivedBase, expectedBase, comparator = {}) {
    const rootPath = ['root'];

    // We clone these because we're going to walk through them and separate any
    // instances where we use a custom comparator
    const compareSets = new Map([[rootPath, {
      type: 'object',
      received: { root: cloneDeep(receivedBase) },
      expected: { root: cloneDeep(expectedBase) },
    }]]);

    const splitForComparator = (path, key, type, a, b) => {
      compareSets.set(path, { type, received: a[key], expected: b[key] });
      delete a[key]; // eslint-disable-line no-param-reassign
      delete b[key]; // eslint-disable-line no-param-reassign
    };

    // We walk through the entire expected vs received, and if we find a value
    // that we have a comparator for, we remove it from the root object into
    // a new scoped chunk, and continue recursing
    const recurse = (parentPath, key, parentA, parentB) => {
      // This lets us grab the correct element to check, where key is null for
      // the root object
      const { a, b } = { a: parentA?.[key], b: parentB?.[key] };
      const path = parentPath.concat(key);

      if (Array.isArray(b)) {
        if (comparator.array) { splitForComparator(path, key, 'array', parentA, parentB); }

        for (let i = 0; i < b.length; i += 1) {
          recurse(path, i, a, b);
        }
      } else if (b.constructor === Object) {
        if (comparator.object) { splitForComparator(path, key, 'object', parentA, parentB); }

        Object.keys(b).forEach((k) => recurse(path, k, a, b));
      } else if (comparator[b.constructor.name]) {
        // This adds support for types like `Date`
        splitForComparator(path, key, b.constructor.name, parentA, parentB);
      } else if (comparator[typeof b] || comparator[b.constructor.name]) {
        // This adds support for types like `number` or `string`
        splitForComparator(path, key, typeof b, parentA, parentB);
      }
    };

    recurse([], 'root', compareSets.get(rootPath).received, compareSets.get(rootPath).expected);

    const getSameParents = (a, b) => {
      const sameParents = [];
      for (let i = 0; i < a.length; i += 1) {
        // If this path segment matches, we still share a parent
        if (a[i] === b[i]) { sameParents.push(a[i]); }
      }

      return sameParents;
    };

    // console.log(compareSets);
    const results = [...compareSets.entries()]
      /**
       * Sort it so that the deepest part of a common path comes first, since we
       * show the first error received, otherwise we'll show an error about the
       * parent before its children in the case of unexpected keys.
       *
       * Wrong order:
       *   Expected: ArrayContaining [{"id": "a"}, {"id": "b"}]
       *   Received: [{"id": "a", "extra": [1]}, {"id": "b", "extra": [2]}]
       *
       *   Expected: ArrayContaining [1]
       *   Received: undefined
       *
       * Right order
       *   Expected: ArrayContaining [{"id": "a"}, {"id": "b"}]
       *   Expected: ArrayContaining [1]
       *
       *   Received: undefined
       *   Received: [{"id": "a", "extra": [1]}, {"id": "b", "extra": [2]}]
       */
      .sort(([a], [b]) => {
        // Don't change the order if they are the same depth
        if (a.length === b.length) { return 0; }

        // If they aren't the same depth, we want to return deeper items first
        return (a.length > getSameParents(a, b).length) ? -1 : 1;
      })
      .map(([path, { type, expected, received }]) => {
        // We use a try-catch to capture any comparators that use
        // `expect(a).[...](b)`, since those will immediately throw, and we want to
        // customize the error output
        try {
          return (comparator[type])
            ? comparator[type](received, expected, path)
            : expect(received).toEqual(expected);
        } catch (e) {
          return {
            ...e.matcherResult,
            // We prefix the error message with the path this error occurred at
            message: () => `Path: ${path.join(' -> ')}\n${e.matcherResult.message()}`,
          };
        }
      })
      .filter((v) => !!v); // Filter out successful matches

    return (results.length === 0)
      ? { pass: !this.isNot }
      : results[0];
  },
});
