import knexReal from 'knex';
import { v4 as uuidv4 } from 'uuid';

const knexClients = [];

const knex = (...v) => {
  const instance = knexReal(...v);
  knexClients.push(instance);
  return instance;
};

const client = (process.env.MYSQL_HOST && 'mysql')
  || (process.env.POSTGRES_USER && 'postgres')
  || 'sqlite';

const connectionOptions = {
  mysql: {
    client: 'mysql2',
    connection: {
      host: process.env.MYSQL_HOST,
      port: `${process.env.MYSQL_PORT || 3306}`.replace(/.*?:?(\d+)$/, '$1'),
      user: 'root',
      password: process.env.MYSQL_ROOT_PASSWORD,
      database: process.env.MYSQL_DATABASE,
    },
  },
  postgres: {
    client: 'postgresql',
    connection: {
      host: process.env.POSTGRES_HOST,
      // Running in CI passes a POSTGRES_PORT of 'tcp://172.17.0.3:5432', so we
      // need to cut off just the port number
      port: `${process.env.POSTGRES_PORT || 5432}`.replace(/.*?:?(\d+)$/, '$1'),
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,
      searchPath: [process.env.POSTGRES_DATABASE, 'public'],
    },
  },
  sqlite: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: { filename: ':memory:' },
  },
};

const getKnexInstance = async () => {
  const dbName = `autocrud_test_${uuidv4().replace(/-/g, '_')}`;
  const baseDb = knex(connectionOptions[client]);

  if (client === 'mysql') {
    await baseDb.raw(`CREATE DATABASE ${dbName}`);
  } else if (client === 'postgres') {
    await baseDb.raw(`CREATE DATABASE "${dbName}"`);
  }

  const instanceOptions = {
    ...connectionOptions[client],
    connection: {
      ...connectionOptions[client].connection,
      database: dbName,
    },
  };

  if (client === 'postgres') {
    instanceOptions.connection.searchPath = [dbName, 'public'];
  }

  const instanceDB = knex(instanceOptions);

  const resetKnexInstance = async () => {
    if (client === 'mysql') {
      await instanceDB.raw('SET foreign_key_checks = 0;');

      const tables = await instanceDB('information_schema.tables')
        .select('table_name')
        .where('table_schema', dbName);

      await tables.reduce((acc, { TABLE_NAME }) => acc.then(
        () => instanceDB(TABLE_NAME).delete(),
      ), Promise.resolve());

      await instanceDB.raw('SET foreign_key_checks = 1;');
    } else if (client === 'postgres') {
      await instanceDB.raw('drop schema if exists public cascade');
      await instanceDB.raw('create schema public');
    } else {
      const tables = await instanceDB('sqlite_master').select('name').where({ type: 'table' });

      await Promise.all(tables.map((t) => instanceDB.schema.dropTableIfExists(t.name)));
    }

    instanceDB.removeAllListeners();
  };

  return {
    knex: instanceDB,
    resetDB: resetKnexInstance,
    destroyKnex: async () => {
      await instanceDB.destroy();

      if (client === 'mysql') {
        await baseDb.raw(`DROP DATABASE IF EXISTS ${dbName}`);
      } else if (client === 'postgres') {
        await baseDb.raw(`DROP DATABASE IF EXISTS "${dbName}"`);
      }

      await baseDb.destroy();
    },
  };
};

export const destroyKnexClients = () => Promise
  .all(knexClients.map((instance) => instance.destroy()));

export default getKnexInstance;
