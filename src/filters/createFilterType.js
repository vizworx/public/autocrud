/* eslint no-underscore-dangle: ["error", { "allow": ["_autocrudInfo", "_filterInfo"] }] */

import {
  isLeafType,
  isListType,
  getNullableType,
  getNamedType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInputObjectType,
  GraphQLUnionType,
} from 'graphql';
import * as allGraphQLTypes from 'graphql/type';
import { FilterMapTypeNotScalar } from '../errors';

const graphQLScalarTypes = Object.entries(allGraphQLTypes).reduce((acc, [name, value]) => {
  if (name.startsWith('GraphQL') && isLeafType(value)) { acc[value.name] = value; }

  return acc;
}, {});

const fieldTypeFilters = {
  ID: ['in'],
  Boolean: [],
  String: ['contains', 'startsWith', 'endsWith', 'in'],
  Int: ['lt', 'lte', 'gt', 'gte', 'in'],
  Float: ['lt', 'lte', 'gt', 'gte', 'in'],
  DateTime: ['lt', 'lte', 'gt', 'gte', 'in'],
};

const addFieldTo = (fields, property, type, operation = 'equals') => {
  const name = (['equals', 'and', 'or', 'relation'].includes(operation))
    ? property
    : `${property}_${operation}`;

  // Don't add the field if it already exists
  if (fields[name]) { return; }

  fields[name] = { // eslint-disable-line no-param-reassign
    name,
    description: undefined,
    type,
    defaultValue: undefined,
    extensions: undefined,
    astNode: undefined,
    _filterInfo: {
      property,
      operation,
    },
  };
};

const createFilterType = (modelType, schema, update = false) => {
  const filterName = `${modelType.name}Filter`;
  let filterType = schema.getType(filterName);

  if (update === false && filterType) { return filterType; }

  filterType = filterType || new GraphQLInputObjectType({ name: filterName, fields: {} });
  schema.getTypeMap()[filterName] = filterType; // eslint-disable-line no-param-reassign

  const filterFields = filterType.getFields();

  const andOr = new GraphQLList(new GraphQLNonNull(filterType));
  addFieldTo(filterFields, 'AND', andOr, 'and');
  addFieldTo(filterFields, 'OR', andOr, 'or');

  // Loop through each field on the model and add the appropriate filters
  Object.values(modelType.getFields()).forEach((modelField) => {
    const modelFieldType = getNamedType(modelField.type);

    // Apply any filterScalarMappings to get the correct type
    let mappedFieldType = modelFieldType;

    if (schema._autocrudInfo.options?.filterScalarMap?.[mappedFieldType.name]) {
      const mappedTypeName = schema._autocrudInfo.options.filterScalarMap[mappedFieldType.name];
      const mappedType = schema.getType(mappedTypeName) || graphQLScalarTypes[mappedTypeName];

      if (mappedType) {
        mappedFieldType = mappedType;

        // If this type isn't being used yet, add it to the schema
        if (!schema.getType(mappedTypeName)) {
          schema.getTypeMap()[mappedTypeName] = mappedType; // eslint-disable-line no-param-reassign
        }
      }
    }

    // TODO: Unions add a bit of complexity, but we should try to support them
    if (modelFieldType instanceof GraphQLUnionType) {
      return;
    }

    // Add suffixes such as _startsWith, _in, and _lte
    (fieldTypeFilters[mappedFieldType.name] || []).forEach((suffix) => {
      addFieldTo(
        filterFields,
        modelField.name,
        suffix === 'in'
          ? new GraphQLList(new GraphQLNonNull(modelFieldType))
          : modelFieldType,
        suffix,
      );
    });

    if (isLeafType(mappedFieldType)) {
      // Add a filter for the exact value
      addFieldTo(filterFields, modelField.name, modelFieldType);
    } else {
      // Only allow scalar types to be mapped
      if (modelFieldType !== mappedFieldType) {
        throw new FilterMapTypeNotScalar(modelType, modelFieldType, mappedFieldType);
      }

      // If we processed this type but skipped the filters because there
      // were no queries, create it now for the relational filter
      if (modelFieldType._filterInfo?.processed) {
        createFilterType(modelFieldType, schema);
      }

      const nestedFilterType = schema.getType(`${modelFieldType.name}Filter`);

      if (nestedFilterType) {
        // Add a filter for relationships
        // If the relationship returns a list, filter some/every/none
        if (isListType(getNullableType(modelField.type))) {
          ['some', 'every', 'none'].forEach((operation) => {
            addFieldTo(
              filterFields,
              modelField.name,
              nestedFilterType,
              operation,
            );
          });
        } else {
          addFieldTo(
            filterFields,
            modelField.name,
            nestedFilterType,
            'relation',
          );
        }
      } else {
        // If we should add a filter but the type doesn't exist, flag it on the
        // type to be returned, and it will be added when that is processed
        if (!modelFieldType._filterInfo) { modelFieldType._filterInfo = {}; }

        const { _filterInfo } = modelFieldType;
        _filterInfo.dependents = (_filterInfo.dependents || []).concat(modelType);
      }
    }
  });

  return filterType;
};

export default createFilterType;
