/* eslint-disable no-underscore-dangle */
import { getNamedType } from 'graphql';
import { v4 as uuidv4 } from 'uuid';

import {
  organizeFields,
  knexUpdateBaseModels,
  knexUpdatePivots,
  aggregateChanges,
} from '../mutations';

import { FilterBuilder } from '../filters';
import { applyKnexOrderBy } from '../orderBy';
import { MissingDataLoaderContext } from '../errors';
import { getQueryName } from '../utilities';

const emitPubSubEvent = async ({
  event,
  mutationName,
  field,
  args,
  context,
  changes,
  currentState,
  prevState,
}) => context.AutoCRUD.pubsub.publish(event, {
  changes,
  mutation: {
    name: mutationName,
    type: field.name,
    table: field._queryInfo.table,
    targetID: args.id,
  },
  currentState,
  prevState,
  changeUUID: uuidv4(),
  metadata: context.AutoCRUD.pubsubMetadata,
});

const resolverBuilders = {};

resolverBuilders.readOne = ({ field }) => async (obj, args, context, info) => {
  if (typeof context.AutoCRUD?.getModelDataLoader !== 'function') {
    throw new MissingDataLoaderContext();
  }

  const preHookArgs = {
    type: getNamedType(field).name,
    table: field._queryInfo.table,
  };

  const resolverArgs = { obj, args, context, info };

  await context.AutoCRUD.resolveHook('preOperation', preHookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('preQuery', preHookArgs, resolverArgs);

  const result = await context.AutoCRUD.getModelDataLoader(field).load(args.id);

  const postHookArgs = {
    ...preHookArgs,
    result,
  };

  await context.AutoCRUD.resolveHook('postQuery', postHookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postOperation', postHookArgs, resolverArgs);

  return result;
};

resolverBuilders.readAll = ({ field, returnType, schema }) => async (obj, args, context, info) => {
  const resolverArgs = { obj, args, context, info };

  const preHookArgs = {
    type: getNamedType(returnType).name,
    table: field._queryInfo.table,
  };

  await context.AutoCRUD.resolveHook('preOperation', preHookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('preQuery', preHookArgs, resolverArgs);

  let query = context.AutoCRUD.knex(`${field._queryInfo.table} as base`)
    .select('base.*');

  if (args.filter) {
    query = new FilterBuilder(context.AutoCRUD.knex, schema)
      .apply(
        query,
        args.filter,
        getNamedType(returnType).name,
      );
  }

  if (args.orderBy) {
    query = applyKnexOrderBy(
      context.AutoCRUD.knex,
      schema,
      query,
      args.orderBy,
    );
  }

  // We check for undefined because we want to support limit(0), even though it
  // is relatively pointless
  if (typeof args.limit !== 'undefined') {
    query = query.limit(args.limit);
  }

  if (args.offset) {
    query = query.offset(args.offset);
  }

  const result = await query;

  const postHookArgs = { ...preHookArgs, result };

  await context.AutoCRUD.resolveHook('postQuery', postHookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postOperation', postHookArgs, resolverArgs);

  return result;
};

resolverBuilders.create = ({
  field,
  inputType,
  argName,
  mutationName,
  schema,
  addChangeInfo,
}) => async (obj, args, context, info) => {
  const resolverArgs = { obj, args, context, info };

  const id = uuidv4();

  const hookArgs = {
    action: 'create',
    id,
    type: getNamedType(field).name,
    table: field._queryInfo.table,
  };

  await context.AutoCRUD.resolveHook('preOperation', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('preMutation', hookArgs, resolverArgs);

  let usingID;
  const fullResult = await context.AutoCRUD.knex.transaction(async (t) => {
    const commitHookArgs = {
      ...hookArgs,
      transaction: t,
    };

    await context.AutoCRUD.resolveHook('preCommit', commitHookArgs, resolverArgs);

    usingID = commitHookArgs.id;

    const fields = organizeFields(
      schema,
      field,
      inputType,
      args[argName],
      usingID,
      t,
    );

    // Wait for any promises to resolve before using the fields
    const result = await Promise
      .all(fields.promises).then(() => {
        // In a create, the record we're creating won't exist yet, so
        // separate it from any other base models that need updates (such as
        // creating a Tag that is the parent of an existing tag).
        const insertRow = fields.base[field._queryInfo.table][usingID];
        insertRow.id = usingID;
        delete fields.base[field._queryInfo.table][usingID];

        return Promise.all([].concat(
          // Insert the new base model
          t(field._queryInfo.table).insert(insertRow).then(() => ({
            [field._queryInfo.table]: { update: [], delete: [], insert: [insertRow] },
          })),
          knexUpdateBaseModels(t, fields), // Update the base models
          knexUpdatePivots(t, fields, usingID), // Upsert the pivots
        ));
      });

    await context.AutoCRUD.resolveHook('postCommit', commitHookArgs, resolverArgs);

    return result;
  });

  // Make sure any subsequent requests aren't using old cached data
  // if a precommit hook loaded it
  context.AutoCRUD.clearAllLoaders();
  const changes = aggregateChanges(fullResult);

  if (changes && context.AutoCRUD.pubsub) {
    const currentState = await context.AutoCRUD
      .getModelDataLoader(field).load(usingID);

    await context.AutoCRUD.resolveHook('prePublish', hookArgs, resolverArgs);

    await emitPubSubEvent({
      event: 'AUTOCRUD.CREATE',
      changes: addChangeInfo(changes),
      mutationName,
      currentState,
      prevState: null,
      field,
      args,
      context,
    });

    await context.AutoCRUD.resolveHook('postPublish', hookArgs, resolverArgs);
  }

  await context.AutoCRUD.resolveHook('postMutation', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postOperation', hookArgs, resolverArgs);

  return schema.getQueryType().getFields()[getQueryName(field)]
    .resolve({}, { id }, context, info);
};

resolverBuilders.update = ({
  field,
  inputType,
  argName,
  mutationName,
  schema,
  addChangeInfo,
}) => async (obj, args, context, info) => {
  const resolverArgs = { obj, args, context, info };

  const hookArgs = {
    action: 'update',
    type: getNamedType(field).name,
    table: field._queryInfo.table,
  };

  await context.AutoCRUD.resolveHook('preOperation', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('preMutation', hookArgs, resolverArgs);

  const prevState = await context.AutoCRUD.getModelDataLoader(field).load(args.id);

  const fullResult = await context.AutoCRUD.knex.transaction(async (t) => {
    const commitHookArgs = {
      ...hookArgs,
      prevState,
      transaction: t,
    };

    await context.AutoCRUD.resolveHook('preCommit', commitHookArgs, resolverArgs);

    const fields = organizeFields(schema, field, inputType, args[argName], args.id, t);

    // Wait for any promises to resolve before using the fields
    const result = await Promise
      .all(fields.promises).then(() => Promise.all([].concat(
        knexUpdateBaseModels(t, fields), // Update the base models
        knexUpdatePivots(t, fields, args.id), // Upsert the pivots
      )));

    await context.AutoCRUD.resolveHook('postCommit', commitHookArgs, resolverArgs);

    return result;
  });

  // Make sure any subsequent requests aren't using old cached data,
  // either from prevState or if a precommit hook loaded it
  context.AutoCRUD.clearAllLoaders();

  const changes = aggregateChanges(fullResult);

  if (changes && context.AutoCRUD.pubsub) {
    const currentState = await context.AutoCRUD.getModelDataLoader(field).load(args.id);

    await context.AutoCRUD.resolveHook('prePublish', hookArgs, resolverArgs);

    await emitPubSubEvent({
      event: 'AUTOCRUD.UPDATE',
      changes: addChangeInfo(changes),
      currentState,
      prevState,
      mutationName,
      field,
      args,
      context,
    });

    await context.AutoCRUD.resolveHook('postPublish', hookArgs, resolverArgs);
  }

  await context.AutoCRUD.resolveHook('postMutation', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postOperation', hookArgs, resolverArgs);

  return schema.getQueryType().getFields()[getQueryName(field)]
    .resolve({}, { id: args.id }, context, info);
};

resolverBuilders.delete = ({
  field,
  mutationName,
}) => async (obj, args, context, info) => {
  const resolverArgs = { obj, args, context, info };

  const hookArgs = {
    action: 'delete',
    type: getNamedType(field).name,
    table: field._queryInfo.table,
  };

  await context.AutoCRUD.resolveHook('preOperation', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('preMutation', hookArgs, resolverArgs);

  // Load the record to make sure there is something to delete, even though
  // we don't use the record
  let prevState;
  await context.AutoCRUD.knex.transaction(async (t) => {
    prevState = await context.AutoCRUD.getModelDataLoader(field, { builder: t })
      .load(args.id);

    const commitHookArgs = {
      ...hookArgs,
      prevState,
      transaction: t,
    };

    await context.AutoCRUD.resolveHook('preCommit', commitHookArgs, resolverArgs);

    // Delete the row
    await t(field._queryInfo.table).delete().where('id', args.id);

    await context.AutoCRUD.resolveHook('postCommit', commitHookArgs, resolverArgs);

    return prevState;
  });

  // Make sure any subsequent requests aren't using old cached data
  // if a precommit hook loaded it
  context.AutoCRUD.clearAllLoaders();

  if (prevState && context.AutoCRUD.pubsub) {
    await context.AutoCRUD.resolveHook('prePublish', hookArgs, resolverArgs);

    await emitPubSubEvent({
      event: 'AUTOCRUD.DELETE',
      changes: [
        {
          operation: 'delete',
          table: field._queryInfo.table,
          type: `${field}`,
          id: args.id,
          values: prevState,
        },
      ],
      mutationName,
      currentState: null,
      prevState,
      field,
      args,
      context,
    });
  }

  await context.AutoCRUD.resolveHook('postPublish', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postMutation', hookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postOperation', hookArgs, resolverArgs);
};

resolverBuilders.interface = ({
  field,
  returnType,
  baseColumns,
  schema,
}) => async (obj, args, context, info) => {
  const resolverArgs = { obj, args, context, info };

  const preHookArgs = {
    type: getNamedType(returnType).name,
    childTypes: field._implementedBy.map(({ name }) => name),
  };

  await context.AutoCRUD.resolveHook('preOperation', preHookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('preQuery', preHookArgs, resolverArgs);

  const selects = field._implementedBy.map(({
    name,
    _queryInfo: { table },
  }) => {
    const columns = baseColumns.map((col) => `${table}.${col}`);

    return context.AutoCRUD.knex(table).select([
      ...columns,
      context.AutoCRUD.knex.raw('? as ??', [name, 'autocrud_model']),
    ]);
  });

  let query = context.AutoCRUD.knex
    .select('base.*')
    .from({ base: selects[0].union(selects.slice(1)) });

  if (args.filter) {
    query = new FilterBuilder(context.AutoCRUD.knex, schema)
      .apply(
        query,
        args.filter,
        getNamedType(returnType).name,
      );
  }

  if (args.orderBy) {
    query = applyKnexOrderBy(
      context.AutoCRUD.knex,
      schema,
      query,
      args.orderBy,
    );
  }

  const result = await query;

  const postHookArgs = {
    ...preHookArgs,
    result,
  };

  await context.AutoCRUD.resolveHook('postQuery', postHookArgs, resolverArgs);
  await context.AutoCRUD.resolveHook('postOperation', postHookArgs, resolverArgs);

  return result;
};

export default (type, params, onCatch) => {
  const resolver = resolverBuilders[type](params);

  return (...args) => resolver(...args).catch(onCatch);
};
