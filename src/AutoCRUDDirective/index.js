// eslint-disable-next-line max-len
/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_filterInfo", "_mutationInfo", "_implementedBy"] }] */
/* eslint-disable max-classes-per-file */

import { SchemaDirectiveVisitor } from '@graphql-tools/utils';
import gql from 'graphql-tag';

import {
  getNamedType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLID,
  GraphQLBoolean,
  GraphQLObjectType,
  GraphQLInt,
} from 'graphql';
import { createFilterType } from '../filters';
import { createOrderByType } from '../orderBy';
import { createInputType } from '../mutations';
import { singularize, camelCaseModel, getQueryName, isScalarField } from '../utilities';
import {
  CreateUpdateNeedsRead,
  MissingIDField,
  MissingTable,
  MissingTypeResolver,
} from '../errors';

import getResolver from './getResolver';

// Adapted from https://stackoverflow.com/a/42755876/9667199
const rethrow = (err, message) => {
  /* eslint-disable no-param-reassign */
  err.extensions = (process.env.NODE_ENV !== 'production')
    ? { stack: err.stack }
    : {};

  err.message = message;
  err.stack = new Error().stack.split('\n').slice(0, 3).join('\n');
  /* eslint-enable no-param-reassign */

  throw err;
};

class AutoCRUDDirectiveStage1 extends SchemaDirectiveVisitor {
  /* eslint-disable no-underscore-dangle */
  ensureAutocrudInfoExists() {
    this.schema._autocrudInfo ||= {};
    this.schema._autocrudInfo.tableModels ||= {};
    return this.schema._autocrudInfo;
  }

  ensureQueryTypeExists() {
    if (!this.schema.getQueryType()) {
      const queryType = new GraphQLObjectType({ name: 'Query', fields: {} });
      this.schema.getTypeMap().Query = queryType;
      // eslint-disable-next-line no-underscore-dangle
      this.schema._queryType = queryType;
    }
  }

  ensureMutationTypeExists() {
    if (!this.schema.getMutationType()) {
      const mutationType = new GraphQLObjectType({ name: 'Mutation', fields: {} });
      this.schema.getTypeMap().Mutation = mutationType;
      // eslint-disable-next-line no-underscore-dangle
      this.schema._mutationType = mutationType;
    }
  }

  // eslint-disable-next-line class-methods-use-this
  visitInterface(interfaceType) {
    // eslint-disable-next-line no-param-reassign
    if (!interfaceType._implementedBy) { interfaceType._implementedBy = []; }
  }

  /* eslint-enable no-underscore-dangle */
  visitObject(field) {
    // We only require the id field if there are mutations and it isn't a pivot input
    if (this.args.create || this.args.update || this.args.delete) {
      if (!this.args.pivotInput && !field.getFields().id) {
        throw new MissingIDField(field);
      }
    }

    field._queryInfo = this.args; // eslint-disable-line no-param-reassign

    if (this.args.readOne || this.args.readAll) {
      this.ensureQueryTypeExists();
    }

    if (this.args.create || this.args.update || this.args.delete) {
      this.ensureMutationTypeExists();
    }

    this.ensureAutocrudInfoExists()
      .tableModels[this.args.table] = field;

    if (!field._filterInfo) { field._filterInfo = {}; } // eslint-disable-line no-param-reassign

    // Inform each interface of what is extending it
    field.getInterfaces().forEach((interfaceType) => {
      // eslint-disable-next-line no-param-reassign
      if (!interfaceType._implementedBy) { interfaceType._implementedBy = []; }

      interfaceType._implementedBy.push(field);
    });
  }
}

class AutoCRUDDirectiveStage2 extends SchemaDirectiveVisitor {
  addChangeInfo = (changes) => {
    changes.forEach((change) => {
      // eslint-disable-next-line no-underscore-dangle
      const changeField = this.schema._autocrudInfo.tableModels[change.table];

      if (!changeField) { return; }

      // eslint-disable-next-line no-param-reassign
      change.type = changeField.name;
    });

    return changes;
  }

  addReadOneQuery(field) {
    const queryFields = this.schema.getQueryType().getFields();
    const queryName = getQueryName(field);

    // Don't create a new query if it already exists
    if (queryFields[queryName]) { return; }

    queryFields[queryName] = {
      type: field,
      args: [{ name: 'id', type: new GraphQLNonNull(GraphQLID) }],
      name: queryName,
      resolve: getResolver(
        'readOne',
        { field },
        (err) => rethrow(err, `Query ${queryName} failed - ${err.message}`),
      ),
      isDeprecated: false,
    };
  }

  addReadAllQuery(field) {
    const queryFields = this.schema.getQueryType().getFields();
    const returnType = new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(field)));
    const queryName = getQueryName(returnType);

    // Don't create a new query if it already exists
    if (queryFields[queryName]) { return; }

    const filterType = createFilterType(field, this.schema);
    const orderByType = createOrderByType(field, this.schema);

    const queryArgs = [
      {
        name: 'filter',
        description: '',
        type: filterType,
      },
      {
        name: 'limit',
        description: '',
        type: GraphQLInt,
      },
      {
        name: 'offset',
        description: '',
        type: GraphQLInt,
      },
    ];

    if (orderByType) {
      queryArgs.push({
        name: 'orderBy',
        description: '',
        type: new GraphQLList(orderByType),
      });
    }

    queryFields[queryName] = {
      type: returnType,
      args: queryArgs,
      name: queryName,
      resolve: getResolver(
        'readAll',
        { field, returnType, schema: this.schema },
        (err) => rethrow(err, `Query '${queryName}' failed - ${err.message}`),
      ),
      isDeprecated: false,
    };
  }

  addCreateMutation(field) {
    const mutationFields = this.schema.getMutationType().getFields();
    const mutationName = `create${field.name}`;
    const inputType = createInputType(field, this.schema);
    const argName = singularize(camelCaseModel(field.name));

    mutationFields[mutationName] = {
      type: new GraphQLNonNull(field),
      args: [{ name: argName, description: '', type: new GraphQLNonNull(inputType) }],
      name: mutationName,
      resolve: getResolver(
        'create',
        {
          field,
          inputType,
          argName,
          mutationName,
          schema: this.schema,
          addChangeInfo: this.addChangeInfo,
        },
        (err) => {
          rethrow(err, `Failed to create ${field.name} - ${err.message}`);
        },
      ),
      isDeprecated: false,
    };
  }

  addUpdateMutation(field) {
    const mutationFields = this.schema.getMutationType().getFields();
    const mutationName = `update${field.name}`;
    const inputType = createInputType(field, this.schema);
    const argName = singularize(camelCaseModel(field.name));

    mutationFields[mutationName] = {
      type: new GraphQLNonNull(field),
      args: [
        { name: 'id', description: '', type: new GraphQLNonNull(GraphQLID) },
        { name: argName, description: '', type: new GraphQLNonNull(inputType) },
      ],
      name: mutationName,
      resolve: getResolver(
        'update',
        {
          field,
          inputType,
          argName,
          mutationName,
          schema: this.schema,
          addChangeInfo: this.addChangeInfo,
        },
        (err) => rethrow(err, `Failed to update ${field.name} - ${err.message}`),
      ),
      isDeprecated: false,
    };
  }

  addDeleteMutation(field) {
    const mutationName = `delete${field.name}`;
    const mutationFields = this.schema.getMutationType().getFields();

    mutationFields[mutationName] = {
      type: GraphQLBoolean,
      args: [{ name: 'id', description: '', type: new GraphQLNonNull(GraphQLID) }],
      name: mutationName,
      resolve: getResolver(
        'delete',
        {
          field,
          mutationName,
        },
        (err) => rethrow(err, `Failed to delete ${field.name} - ${err.message}`),
      ),
      isDeprecated: false,
    };
  }

  visitObject(field) {
    if (!this.args.table) {
      throw new MissingTable(field);
    }

    if (this.args.readOne) { this.addReadOneQuery(field); }

    if (this.args.readAll) { this.addReadAllQuery(field); }

    if (!this.args.readOne && (this.args.create || this.args.update)) {
      throw new CreateUpdateNeedsRead(field);
    }

    if (this.args.create) { this.addCreateMutation(field); }

    if (this.args.update) { this.addUpdateMutation(field); }

    if (this.args.delete) { this.addDeleteMutation(field); }

    // If another type flagged that they want to filter by this, ensure the
    // filter exists and update the dependents
    if ((field._filterInfo.dependents || []).length > 0) {
      createFilterType(field, this.schema);
      field._filterInfo.dependents.forEach((dependent) => {
        createFilterType(dependent, this.schema, true);
      });
    }

    field._filterInfo.processed = true; // eslint-disable-line no-param-reassign
  }

  visitInterface(field) {
    if (!field.resolveType) {
      throw new MissingTypeResolver(field);
    }

    const queryFields = this.schema.getQueryType().getFields();
    const returnType = new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(field)));
    const queryName = getQueryName(returnType);

    // Don't create a new query if it already exists
    if (queryFields[queryName]) { return; }

    const baseColumns = Object.values(field.getFields())
      .filter(isScalarField)
      .map((f) => getNamedType(f).name);

    queryFields[queryName] = {
      type: returnType,
      args: [
        {
          name: 'filter',
          description: '',
          type: createFilterType(field, this.schema),
        },
        {
          name: 'orderBy',
          description: '',
          type: new GraphQLList(createOrderByType(field, this.schema)),
        },
      ],
      name: queryName,
      resolve: getResolver(
        'interface',
        {
          field,
          returnType,
          schema: this.schema,
          baseColumns,
        },
        (err) => rethrow(err, `Failed to query interface ${field.name} - ${err.message}`),
      ),
      isDeprecated: false,
    };
  }
}

class ExcludeFromInputDirective extends SchemaDirectiveVisitor {
  // eslint-disable-next-line class-methods-use-this
  visitFieldDefinition(field) {
    // eslint-disable-next-line no-param-reassign, no-underscore-dangle
    field._excludeFromInput = true;
  }
}

export const directiveStages = {
  autocrud: [
    AutoCRUDDirectiveStage1,
    AutoCRUDDirectiveStage2,
  ],
  excludeFromInput: [
    ExcludeFromInputDirective,
  ],
};

export const typeDefs = gql`
  directive @autocrud(
    table: String,
    pivotInput: String = "",
    readAll: Boolean = true,
    readOne: Boolean = true,
    create: Boolean = true,
    update: Boolean = true,
    delete: Boolean = true,
  ) on OBJECT | INTERFACE

  directive @excludeFromInput on FIELD_DEFINITION
`;
