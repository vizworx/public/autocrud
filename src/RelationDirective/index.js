// eslint-disable-next-line max-len
/* eslint no-underscore-dangle: ["error", { "allow": ["_relationInfo", "_queryInfo", "_implementedBy"] }] */
/* eslint-disable max-classes-per-file */

import gql from 'graphql-tag';
import { SchemaDirectiveVisitor } from '@graphql-tools/utils';
import { isListType, getNamedType, getNullableType } from 'graphql';
import { RelationObjectMissingError, RelationAllQueryMissingError } from '../errors';
import { getRelationInfo, getQueryName } from '../utilities';

class RelationDirectiveStage1 extends SchemaDirectiveVisitor {
  visitFieldDefinition(field, details) {
    if (!this.args.from && !this.args.to && !this.args.through) {
      throw new RelationObjectMissingError(field, ['from', 'to', 'through']);
    }

    // eslint-disable-next-line no-param-reassign
    field._relationInfo = {
      ...this.args,
      baseModel: details.objectType,
    };
  }
}

class RelationDirectiveStage2 extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    // Don't override existing resolvers
    if (field.resolve) { return; }

    // eslint-disable-next-line no-param-reassign
    field.resolve = async (obj, args, context, info) => {
      const isList = isListType(getNullableType(field.type));
      const queries = this.schema.getType('Query').getFields();
      const queryName = getQueryName(field.type, true);
      const relationAllQuery = queries[queryName];

      if (!relationAllQuery) {
        throw new RelationAllQueryMissingError(field, queryName, info);
      }

      const relation = getRelationInfo(field);
      let relationIn = [].concat(obj[relation.fromColumn]);

      if (relation.through) {
        relationIn = await context.AutoCRUD
          .getTableDataLoader(
            relation.through.table,
            {
              field: relation.through.near,
              manyResults: true,
            },
          )
          .load(obj[relation.from])
          .then((result) => result.map((v) => v[relation.through.far]));
      }

      relationIn = relationIn.filter((v) => !!v); // Filter null relations

      if (relationIn.length === 0) { return isList ? [] : null; }

      // You cannot have an interface that resolves to a single record
      if (!isList) {
        const allResult = await relationAllQuery.resolve(
          {},
          { filter: { [relation.to]: relationIn[0] } },
          context,
          info,
        );

        return allResult?.[0] ?? null;
      }

      // Handle interfaces
      const fieldNamedType = getNamedType(field.type);
      const models = fieldNamedType._implementedBy ?? [fieldNamedType];

      return Promise.all(models.map((fieldModel) => context.AutoCRUD
        .getModelDataLoader(fieldModel, {
          field: relation.toColumn,
          manyResults: true,
        })
        .loadMany(relationIn)
        // X-to-one and X-to-many relationships return in different formats,
        // so we flatten [[TextRow]] into [TextRow] to ensure both work
        .then((result) => result.flat().map((record) => ({
          ...record,
          autocrud_model: fieldModel.name,
        })))))
        .then((result) => result.flat());
    };
  }
}

export const directiveStages = {
  relation: [
    RelationDirectiveStage1,
    RelationDirectiveStage2,
  ],
};

export const typeDefs = gql`
  directive @relation(
    table: String,
    from: String,
    to: String,
    through: RelationThrough
  ) on FIELD_DEFINITION

  input RelationThrough {
    near: String!
    table: String!
    far: String!
  }
`;
