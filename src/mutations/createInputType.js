/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_mutationInfo"] }] */

import {
  isLeafType,
  isListType,
  getNullableType,
  getNamedType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInputObjectType,
  GraphQLID,
} from 'graphql';
import { singularize } from '../utilities';
import { DuplicateMutationInputType } from '../errors';

const addFieldTo = (fields, name, type, forField) => {
  fields[name] = { // eslint-disable-line no-param-reassign
    name,
    description: undefined,
    type,
    defaultValue: undefined,
    extensions: undefined,
    astNode: undefined,
    _mutationInfo: {
      forField,
    },
  };
};

const createInputType = (modelType, schema) => {
  const mutationName = `${modelType.name}Params`;
  let mutationType = schema.getType(mutationName);

  if (mutationType) {
    if (!mutationType._mutationInfo?.processed) {
      throw new DuplicateMutationInputType(mutationName);
    }

    // If we've already processed this type for create or update, skip it
    return mutationType;
  }

  mutationType = new GraphQLInputObjectType({ name: mutationName, fields: {} });
  mutationType._mutationInfo = { processed: true };
  schema.getTypeMap()[mutationName] = mutationType; // eslint-disable-line no-param-reassign

  const mutationFields = mutationType.getFields();

  // Loop through each field on the model and add the appropriate mutations
  Object.values(modelType.getFields()).forEach((modelField) => {
    // eslint-disable-next-line no-underscore-dangle
    if (modelField._excludeFromInput) { return; }

    const modelFieldType = getNamedType(modelField.type);

    // Filter out fields that shouldn't be mutated or are handled internally
    if (['id', 'createdAt', 'updatedAt'].includes(modelField.name)) { return; }

    if (modelField.name.endsWith('ID')) { return; }

    // Filter out fields that have a custom resolver
    if (modelField.resolve && !modelField.resolve.autocrudInternal) { return; }

    if (isLeafType(modelFieldType)) {
      // Add a mutation for the exact value (string, bool, int, etc)
      addFieldTo(mutationFields, modelField.name, modelField.type, modelField);
    } else if (isListType(getNullableType(modelField.type))) {
      const pivotType = modelFieldType._queryInfo?.pivotInput
        // [Type@autocrud(pivotInput:"WeightedID")!]=[WeightedID!]
        ? schema.getType(modelFieldType._queryInfo?.pivotInput)
        // [Type]=[ID!]
        : GraphQLID;

      addFieldTo(
        mutationFields,
        `${singularize(modelField.name)}IDs`,
        // We don't mark x-to-many relationships as required because there is no
        // difference between providing `[]` or not providing it in the end
        // result, since both will still return `[]`
        new GraphQLList(new GraphQLNonNull(pivotType)),
        modelField,
      );
    } else {
      // Type=ID
      addFieldTo(
        mutationFields,
        `${modelField.name}ID`,
        // If the field with the relation is required, mark it as required in the params
        modelField.type instanceof GraphQLNonNull
          ? new GraphQLNonNull(GraphQLID)
          : GraphQLID,
        modelField,
      );
    }
  });

  return mutationType;
};

export default createInputType;
