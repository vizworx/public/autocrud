// eslint-disable-next-line max-len
/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_mutationInfo", "_dbColumn"] }] */

import { getNamedType, isLeafType, GraphQLID } from 'graphql';
import { getRelationInfo, mapFieldToDBColumn } from '../utilities';
import {
  MissingQueriesDirective,
  MissingPivotInputType,
  UnexpectedIDsOnPivot,
  PivotInputMismatchedFields,
  UnknownRelationType,
} from '../errors';

/* eslint-disable no-param-reassign */
const accTableFields = (acc, table, id, key, value) => {
  if (!acc.base[table]) { acc.base[table] = {}; }

  if (!acc.base[table][id]) { acc.base[table][id] = {}; }

  acc.base[table][id][key] = value;
  return acc;
};

const accRelationship = (acc, table, values) => {
  acc.upsert[table] = (acc.upsert[table] || []).concat(values);
  return acc;
};
/* eslint-enable no-param-reassign */

const organizeFields = (schema, field, inputType, data, id, t) => Object.entries(data)
  .reduce((acc, [k, v]) => {
    const forField = inputType.getFields()[k]?._mutationInfo?.forField;

    // Field is defined as a scalar, or it's not included in the schema so we
    // can treat it like one.
    if (!forField || isLeafType(getNamedType(forField.type))) {
      const dbKey = mapFieldToDBColumn(field, k);

      return accTableFields(acc, field._queryInfo.table, id, dbKey, v);
    }

    const relation = getRelationInfo(forField);

    // One-to-____ relationship coming from this record (tag.childOf)
    if (relation.from !== 'id') {
      return accTableFields(acc, field._queryInfo.table, id, relation.fromColumn, v);
    }

    // Many-to-many with pivot type
    const { pivotInput: pivotInputName } = getNamedType(forField.type)?._queryInfo ?? {};

    if (pivotInputName) {
      const pivotInput = schema.getType(pivotInputName);

      if (!pivotInput) { throw new MissingPivotInputType(pivotInputName); }

      // Go through all of the fields on the resulting type and copy the
      // ones that match, then add in the relation.to, and try to map the remaining ID
      const { table } = getNamedType(forField.type)._queryInfo;
      const pivotFields = Object.values(getNamedType(forField.type).getFields());

      // Determine which ID columns are used in the pivot
      if (!acc.pivotKeys[table]) {
        const remainingIDs = pivotFields.filter((pivotField) => {
          if (getNamedType(pivotField.type) !== GraphQLID) { return false; }

          if (pivotField.name === relation.to) { return false; }

          return true;
        });

        if (remainingIDs.length !== 1) {
          throw new UnexpectedIDsOnPivot(
            forField.type,
            // Put the key that we filtered out back on
            remainingIDs.map(({ name }) => name).concat(relation.to),
          );
        }

        acc.pivotKeys[table] = {
          near: relation.toColumn,
          far: mapFieldToDBColumn(forField.type, remainingIDs[0].name),
        };
      }

      const pivotRows = v.map((row) => {
        const rowValue = {};
        pivotFields.forEach((pivotField) => {
          if (typeof row[pivotField.name] !== 'undefined') {
            rowValue[pivotField.name] = row[pivotField.name];
          }
        });

        const { near, far } = acc.pivotKeys[table];
        rowValue[near] = id;
        rowValue[far] = row.id;
        const remainingValues = Object.keys(row)
          .filter((key) => (key !== 'id' && typeof rowValue[key] === 'undefined'));

        if (remainingValues.length !== 0) {
          throw new PivotInputMismatchedFields(
            pivotInput,
            getNamedType(forField.type),
            remainingValues,
          );
        }

        return rowValue;
      });

      return accRelationship(acc, table, pivotRows);
    }

    // Many-to-many without pivot type
    if (relation.through) {
      // Near and Far are DB columns, so we don't need to mapFieldToDBColumn
      const { near, far, table } = relation.through;
      acc.pivotKeys[table] = { near, far };
      const pivotRows = v.map((farID) => ({ [near]: id, [far]: farID }));

      return accRelationship(acc, table, pivotRows);
    }

    // One-to-many relationship going to other record (tag.parentOf)
    if (relation.to !== 'id') {
      const targetType = getNamedType(forField.type);
      const targetTable = targetType._queryInfo?.table;

      if (!targetTable) {
        throw new MissingQueriesDirective(getNamedType(forField.type));
      }

      // For each ID, flag that we need to update the far record with this record's ID
      v.forEach((farID) => accTableFields(acc, targetTable, farID, relation.toColumn, id));

      // For the items that are currently assigned, we need to remove the relationship
      const currentFarQuery = t(targetTable)
        .select('id')
        .where(relation.toColumn, id)
        .whereNotIn('id', v)
        .then((currentFar) => currentFar
          .forEach((far) => accTableFields(acc, targetTable, far.id, relation.toColumn, null)));

      acc.promises.push(currentFarQuery);

      return acc;
    }

    // TODO: This doesn't support one-to-one relationships going "to"
    // another record. It only supports one-to-many going to. This is
    // because we don't currently have any one-to-one relationships
    // to test this with.
    throw new UnknownRelationType(field, forField);
  }, { base: {}, upsert: {}, pivotKeys: {}, promises: [] });

export default organizeFields;
