const noop = () => {};

export default (hooks) => async (hookPoint, hookArgs, resolverArgs) => {
  if (!hooks) { return noop; }

  const toRun = [].concat(hooks[hookPoint], hooks[hookArgs.type]?.[hookPoint]).filter(Boolean);

  return toRun.reduce(
    (acc, next) => acc.then(async () => next(hookArgs, resolverArgs)),
    Promise.resolve(),
  );
};
