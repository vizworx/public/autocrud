/* eslint no-underscore-dangle: ["error", { "allow": ["_autocrudInfo"] }] */
import { directiveStages as RelationDirectiveStages, typeDefs as autoCRUDTypeDefs } from './RelationDirective';
import { directiveStages as AutoCRUDDirectiveStages, typeDefs as relationTypeDefs } from './AutoCRUDDirective';
import { directiveStages as DBColumnDirectiveStages, typeDefs as dbColumnTypeDefs } from './DBColumnDirective';
import getHookResolver from './hooks';
import createRequestDataLoaders from './createRequestDataLoaders';
import { applyStagedDirectives } from './utilities';

export const initialize = (schema, options = {}) => {
  /* eslint-disable no-param-reassign */
  schema._autocrudInfo ||= {};
  schema._autocrudInfo.options = options;
  /* eslint-enable no-param-reassign */

  applyStagedDirectives(schema, {
    ...RelationDirectiveStages,
    ...AutoCRUDDirectiveStages,
    ...DBColumnDirectiveStages,
  });

  return {
    getContext: ({
      knex,
      pubsub = null,
      pubsubMetadata = null,
      hooks = null,
    }) => ({
      AutoCRUD: {
        knex,
        pubsub,
        pubsubMetadata,
        resolveHook: getHookResolver(hooks),
        ...createRequestDataLoaders(knex, schema),
      },
    }),
  };
};

export const typeDefs = [
  autoCRUDTypeDefs,
  relationTypeDefs,
  dbColumnTypeDefs,
];
