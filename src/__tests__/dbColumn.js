import gql from 'graphql-tag';

import AutoCRUDTester from '../testUtils/AutoCRUDTester';
import { DBColumnIDError } from '../errors';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';
import { directiveStages as DBColumnDirectiveStages, typeDefs as dbColumnTypeDefs } from '../DBColumnDirective';

const typeDefs = gql`
  type Author @autocrud(table: "authors") {
    id: ID!
    name: String @dbColumn(name: "author_name")
  }

  type Book @autocrud(table: "books") {
    id: ID!
    chapters: [Chapter!]! @relation(to: "bookID")
    authorID: ID! @dbColumn(name: "author_id")
    author: Author! @relation(from: "authorID")
  }

  type Chapter @autocrud(table: "book_chapters") {
    id: ID!
    bookID: ID! @dbColumn(name: "book_id")
    book: Book! @relation(from: "bookID")
  }

  type BookClub @autocrud(table: "book_clubs") {
    id: ID!
    name: String!
    books: [Book!]! @relation(through: { near: "book_club_id", table: "book_club_books", far: "book_id" })
  }
`;

describe('DBColumnDirective', () => {
  describe('invalid usage', () => {
    test('@dbColumn on the id field should throw', () => {
      expect.assertions(1);
      const schemaThrows = Promise.resolve().then(() => new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          dbColumnTypeDefs,
          gql`
            type Book @autocrud(table: "books") {
              id: ID! @dbColumn(name: "test")
            }
          `,
        ],
        directives: {
          ...AutoCRUDDirectiveStages,
          ...DBColumnDirectiveStages,
        },
      }));

      expect(schemaThrows).rejects.toThrow(DBColumnIDError);
    });
  });

  describe('valid usage', () => {
    let tester;
    beforeAll(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          relationTypeDefs,
          dbColumnTypeDefs,
          typeDefs,
        ],
        directives: {
          ...AutoCRUDDirectiveStages,
          ...RelationDirectiveStages,
          ...DBColumnDirectiveStages,
        },
      });

      const { knex } = tester;

      await knex.schema.createTable('authors', (t) => {
        t.uuid('id').unique();
        t.string('author_name');
      });

      await knex.schema.createTable('books', (t) => {
        t.uuid('id').unique();
        t.uuid('author_id').references('authors.id');
      });

      await knex.schema.createTable('book_chapters', (t) => {
        t.uuid('id').unique();
        t.uuid('book_id').references('books.id');
      });

      await knex.schema.createTable('book_clubs', (t) => {
        t.uuid('id').unique();
        t.string('name');
      });

      await knex.schema.createTable('book_club_books', (t) => {
        t.uuid('book_club_id');
        t.uuid('book_id');
      });

      await knex('authors').insert([
        { id: tester.uuid('Author 1'), author_name: 'Charles Dickens' },
        { id: tester.uuid('Author 2'), author_name: 'Mark Twain' },
      ]);

      await knex('books').insert([
        { id: tester.uuid('Book 1'), author_id: tester.uuid('Author 1') },
        { id: tester.uuid('Book 2'), author_id: tester.uuid('Author 2') },
      ]);

      await knex('book_chapters').insert([
        { id: tester.uuid('Book 1 - Chapter 1'), book_id: tester.uuid('Book 1') },
        { id: tester.uuid('Book 1 - Chapter 2'), book_id: tester.uuid('Book 1') },
        { id: tester.uuid('Book 1 - Chapter 3'), book_id: tester.uuid('Book 1') },
        { id: tester.uuid('Book 2 - Chapter 1'), book_id: tester.uuid('Book 2') },
        { id: tester.uuid('Book 2 - Chapter 2'), book_id: tester.uuid('Book 2') },
      ]);

      await knex('book_clubs').insert({ id: tester.uuid('Club 1'), name: 'Test Club' });

      await knex('book_club_books').insert([
        { book_club_id: tester.uuid('Club 1'), book_id: tester.uuid('Book 1') },
      ]);
    });

    test('@dbColumn(name: "book_id")', async () => {
      const graphqlResult = await tester.query(`
        query {
          allChapters { id bookID }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allChapters: [
            { id: tester.uuid('Book 1 - Chapter 1'), bookID: tester.uuid('Book 1') },
            { id: tester.uuid('Book 1 - Chapter 2'), bookID: tester.uuid('Book 1') },
            { id: tester.uuid('Book 1 - Chapter 3'), bookID: tester.uuid('Book 1') },
            { id: tester.uuid('Book 2 - Chapter 1'), bookID: tester.uuid('Book 2') },
            { id: tester.uuid('Book 2 - Chapter 2'), bookID: tester.uuid('Book 2') },
          ],
        },
      });
    });

    test('@dbColumn with a @relation(from)', async () => {
      const graphqlResult = await tester.query(`
        query {
          allBooks { id author { id name } }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allBooks: [
            {
              id: tester.uuid('Book 1'),
              author: {
                id: tester.uuid('Author 1'),
                name: 'Charles Dickens',
              },
            },
            {
              id: tester.uuid('Book 2'),
              author: {
                id: tester.uuid('Author 2'),
                name: 'Mark Twain',
              },
            },
          ],
        },
      });
    });

    test('@dbColumn through relationship', async () => {
      const graphqlResult = await tester.query(`
        query {
          allBooks { id chapters { id bookID } }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allBooks: [
            {
              id: tester.uuid('Book 1'),
              chapters: [
                { id: tester.uuid('Book 1 - Chapter 1'), bookID: tester.uuid('Book 1') },
                { id: tester.uuid('Book 1 - Chapter 2'), bookID: tester.uuid('Book 1') },
                { id: tester.uuid('Book 1 - Chapter 3'), bookID: tester.uuid('Book 1') },
              ],
            },
            {
              id: tester.uuid('Book 2'),
              chapters: [
                { id: tester.uuid('Book 2 - Chapter 1'), bookID: tester.uuid('Book 2') },
                { id: tester.uuid('Book 2 - Chapter 2'), bookID: tester.uuid('Book 2') },
              ],
            },
          ],
        },
      });
    });

    test('@dbColumn in a nested @relation filter', async () => {
      const graphqlResult = await tester.query(
        `
          query ($filter: BookClubFilter) {
            allBookClubs(filter: $filter) {
              id
              name
              books { id }
            }
          }
        `,
        {
          filter: {
            books_some: {
              chapters_some: {
                id: tester.uuid('Book 1 - Chapter 1'),
              },
            },
          },
        },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allBookClubs: [
            {
              id: tester.uuid('Club 1'),
              name: 'Test Club',
              books: [
                { id: tester.uuid('Book 1') },
              ],
            },
          ],
        },
      });
    });

    test('@dbColumn with a filter', async () => {
      const graphqlResult = await tester.query(
        `
          query ($filter: ChapterFilter) {
            allChapters(filter: $filter) { id bookID }
          }
        `,
        { filter: { bookID: tester.uuid('Book 2') } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allChapters: [
            { id: tester.uuid('Book 2 - Chapter 1'), bookID: tester.uuid('Book 2') },
            { id: tester.uuid('Book 2 - Chapter 2'), bookID: tester.uuid('Book 2') },
          ],
        },
      });
    });

    test('filter on a @relation(from) on a @dbColumn', async () => {
      const graphqlResult = await tester.query(
        `
          query ($filter: BookFilter) {
            allBooks(filter: $filter) { id author { name } }
          }
        `,
        { filter: { author: { name: 'Charles Dickens' } } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allBooks: [
            { id: tester.uuid('Book 1'), author: { name: 'Charles Dickens' } },
          ],
        },
      });
    });

    test('@dbColumn in a mutation', async () => {
      const graphqlResult = await tester.query(
        `
          mutation($id: ID!, $author: AuthorParams!) {
            updateAuthor(id: $id, author: $author) { id name }
          }
        `,
        { id: tester.uuid('Author 2'), author: { name: 'Ernest Hemingway' } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          updateAuthor: {
            id: tester.uuid('Author 2'),
            name: 'Ernest Hemingway',
          },
        },
      });

      const result = await tester.knex('authors').where({ id: tester.uuid('Author 2') }).first();
      expect(result?.author_name).toEqual('Ernest Hemingway'); // eslint-disable-line camelcase
    });

    test('@dbColumn with a @relation(from: "bookID") mutation', async () => {
      const graphqlResult = await tester.query(
        `
          mutation($id: ID!, $chapter: ChapterParams!) {
            updateChapter(id: $id, chapter: $chapter) { id bookID }
          }
        `,
        { id: tester.uuid('Book 1 - Chapter 3'), chapter: { bookID: tester.uuid('Book 2') } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          updateChapter: {
            id: tester.uuid('Book 1 - Chapter 3'),
            bookID: tester.uuid('Book 2'),
          },
        },
      });

      const result = await tester.knex('book_chapters').where({ id: tester.uuid('Book 1 - Chapter 3') }).first();
      expect(result?.book_id).toEqual(tester.uuid('Book 2')); // eslint-disable-line camelcase
    });

    test('@dbColumn with a @relation(to: "bookID") mutation', async () => {
      const graphqlResult = await tester.query(
        `
          mutation($id: ID!, $book: BookParams!) {
            updateBook(id: $id, book: $book) { id chapters { id bookID } }
          }
        `,
        {
          id: tester.uuid('Book 1'),
          book: {
            chapterIDs: [
              tester.uuid('Book 1 - Chapter 1'),
              tester.uuid('Book 1 - Chapter 2'),
              tester.uuid('Book 2 - Chapter 2'),
            ],
            // This isn't changing but is a required field
            authorID: tester.uuid('Author 1'),
          },
        },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          updateBook: {
            id: tester.uuid('Book 1'),
            chapters: [
              { id: tester.uuid('Book 1 - Chapter 1'), bookID: tester.uuid('Book 1') },
              { id: tester.uuid('Book 1 - Chapter 2'), bookID: tester.uuid('Book 1') },
              { id: tester.uuid('Book 2 - Chapter 2'), bookID: tester.uuid('Book 1') },
            ],
          },
        },
      });

      const result = await tester.knex('book_chapters').where({ book_id: tester.uuid('Book 1') });
      expect(result).toEqual([
        { id: tester.uuid('Book 1 - Chapter 1'), book_id: tester.uuid('Book 1') },
        { id: tester.uuid('Book 1 - Chapter 2'), book_id: tester.uuid('Book 1') },
        { id: tester.uuid('Book 2 - Chapter 2'), book_id: tester.uuid('Book 1') },
      ]);
    });

    test('@dbColumn with a @relation(through) mutation', async () => {
      const graphqlResult = await tester.query(
        `
          mutation($bookClub: BookClubParams!) {
            createBookClub(bookClub: $bookClub) { name books { id } }
          }
        `,
        { bookClub: { name: 'My book club', bookIDs: [tester.uuid('Book 2')] } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          createBookClub: {
            name: 'My book club',
            books: [
              { id: tester.uuid('Book 2') },
            ],
          },
        },
      });
    });
  });
});
