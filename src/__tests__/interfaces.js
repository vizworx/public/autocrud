import gql from 'graphql-tag';
import { validate, getIntrospectionQuery } from 'graphql';

import testGraphql from '../testUtils/expect-graphql';
import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';

const interfaceFields = `
  id: ID!
  name: String!
  ownerID: ID
  owner: Person @relation(from: "ownerID")
`;

const typeDefs = gql`
  interface Pet @autocrud {
    ${interfaceFields}
  }

  type Cat implements Pet @autocrud(
    table: "cats",
  ) {
    ${interfaceFields}
    likesBeingPet: Boolean!
  }

  type Dog implements Pet @autocrud(
    table: "dogs",
  ) {
    ${interfaceFields}
    favoriteToy: String!
  }

  type Person @autocrud(table: "people") {
    id: ID!
    name: String!
    phone: String!
    email: String!
    age: Int!
    pets: [Pet!]! @relation(to: "ownerID")
  }
`;

const resolvers = {
  Pet: {
    __resolveType: (obj) => obj.autocrud_model,
  },
};

describe('interfaces', () => {
  let tester;
  let data;

  beforeAll(async () => {
    tester = await new AutoCRUDTester({
      typeDefs: [
        autoCRUDTypeDefs,
        relationTypeDefs,
        typeDefs,
      ],
      resolvers,
      directives: {
        ...AutoCRUDDirectiveStages,
        ...RelationDirectiveStages,
      },
    });

    data = {
      cats: [
        {
          id: tester.uuid('cat-1'),
          name: 'Fluffy',
          ownerID: tester.uuid('person-1'),
          likesBeingPet: true,
        },
        {
          id: tester.uuid('cat-2'),
          name: 'Nelly',
          ownerID: tester.uuid('person-1'),
          likesBeingPet: false,
        },
        {
          id: tester.uuid('cat-3'),
          name: 'Bagheera',
          ownerID: null,
          likesBeingPet: false,
        },
      ],
      dogs: [
        {
          id: tester.uuid('dog-1'),
          name: 'Fido',
          ownerID: null,
          favoriteToy: 'Slipper',
        },
        {
          id: tester.uuid('dog-2'),
          name: 'Wishbone',
          ownerID: tester.uuid('person-2'),
          favoriteToy: 'Squeaky football',
        },
      ],
      people: [
        {
          id: tester.uuid('person-1'),
          name: 'John',
          phone: '555-213-5252',
          email: 'coolio@net.com',
          age: 37,
        },
        {
          id: tester.uuid('person-2'),
          name: 'Anne',
          phone: null,
          email: null,
          age: 23,
        },
      ],
    };

    await tester.knex.schema.createTable('cats', (table) => {
      table.uuid('id').unique();
      table.text('name');
      table.uuid('ownerID');
      table.bool('likesBeingPet');
    });

    await tester.knex.schema.createTable('dogs', (table) => {
      table.uuid('id').unique();
      table.text('name');
      table.uuid('ownerID');
      table.string('favoriteToy');
    });

    await tester.knex.schema.createTable('people', (table) => {
      table.uuid('id').unique();
      table.text('name');
      table.string('phone');
      table.string('email');
      table.smallint('age');
    });

    await tester.knex('people').insert(data.people);
    await tester.knex('cats').insert(data.cats);
    await tester.knex('dogs').insert(data.dogs);
  });

  describe('defaults', () => {
    test('schema has no errors', () => {
      const schemaErrors = validate(tester.schema, gql`${getIntrospectionQuery()}`);

      expect(schemaErrors).toEqual([]);
    });

    test('base query', async () => {
      const result = await tester.query(`
        query { allPets { name __typename } }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPets).toEqual(expect.arrayContaining([
        { name: 'Fluffy', __typename: 'Cat' },
        { name: 'Nelly', __typename: 'Cat' },
        { name: 'Bagheera', __typename: 'Cat' },
        { name: 'Fido', __typename: 'Dog' },
        { name: 'Wishbone', __typename: 'Dog' },
      ]));
    });

    test('with relations', async () => {
      const result = await tester.query(`
        query {
          allPets {
            __typename
            owner {
              name
              __typename
            }
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPets).toEqual(expect.arrayContaining([
        { owner: { name: 'John', __typename: 'Person' }, __typename: 'Cat' },
        { owner: { name: 'John', __typename: 'Person' }, __typename: 'Cat' },
        { owner: null, __typename: 'Cat' },
        { owner: null, __typename: 'Dog' },
        { owner: { name: 'Anne', __typename: 'Person' }, __typename: 'Dog' },
      ]));
    });
  });

  describe('filtering', () => {
    testGraphql.object(() => tester.schema.getType('PetFilter'))
      .shouldOnlyHaveFields({
        AND: '[PetFilter!]',
        OR: '[PetFilter!]',
        id: 'ID',
        id_in: '[ID!]',
        name: 'String',
        name_contains: 'String',
        name_endsWith: 'String',
        name_in: '[String!]',
        name_startsWith: 'String',
        owner: 'PersonFilter',
        ownerID: 'ID',
        ownerID_in: '[ID!]',
      });

    test('by value', async () => {
      const result = await tester.query(`
        query {
          allPets(filter: { name_contains: "a" }) {
            name
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPets).toEqual([
        { name: 'Bagheera' },
      ]);
    });

    test('by relation', async () => {
      const result = await tester.query(`
        query {
          allPets(
            filter: { owner: { name_contains: "A" } }
          ) {
            name
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPets).toEqual([
        { name: 'Wishbone' },
      ]);
    });

    test('by relation to interface', async () => {
      const result = await tester.query(`
        query {
          allPersons(
            filter: { pets_some: { name_contains: "o" } }
          ) {
            name
            pets { name }
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPersons).toEqual([
        { name: 'Anne', pets: [{ name: 'Wishbone' }] },
      ]);
    });
  });

  describe('ordering', () => {
    testGraphql.object(() => tester.schema.getType('PetOrderBy'))
      .shouldOnlyHaveFields({
        desc: 'Boolean',
        field: 'PetOrderByFields',
      });

    testGraphql.object(() => tester.schema.getType('PetOrderByFields'))
      .shouldOnlyHaveEnumKeys([
        'name',
        'owner_age',
        'owner_email',
        'owner_name',
        'owner_phone',
      ]);

    test('by value', async () => {
      const result = await tester.query(`
        query {
          allPets(
            orderBy: [{ field: name }]
          ) {
            name
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPets).toEqual([
        { name: 'Bagheera' },
        { name: 'Fido' },
        { name: 'Fluffy' },
        { name: 'Nelly' },
        { name: 'Wishbone' },
      ]);
    });

    test('by value (descending)', async () => {
      const result = await tester.query(`
        query {
          allPets(
            orderBy: [{ field: name, desc: true }]
          ) {
            name
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPets).toEqual([
        { name: 'Wishbone' },
        { name: 'Nelly' },
        { name: 'Fluffy' },
        { name: 'Fido' },
        { name: 'Bagheera' },
      ]);
    });

    test('by relation', async () => {
      const result = await tester.query(`
        query {
          allPets(
            orderBy: [{ field: owner_age }]
          ) {
            name
            owner { age }
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      const ages = result?.data?.allPets.map((v) => v.owner?.age || null);
      expect(ages).toEqual([null, null, 23, 37, 37]);
    });

    test('by relation to interface', async () => {
      const result = await tester.query(`
        query {
          allPersons(orderBy: [
            { field: pets_name },
          ]) {
            name
            pets { name }
          }
        }
      `);

      expect(result).graphqlResultNotToHaveErrors();
      expect(result?.data?.allPersons).toEqual([
        {
          name: 'John',
          pets: [
            // The pets within the person are not ordered
            { name: 'Fluffy' },
            { name: 'Nelly' },
          ],
        },
        {
          name: 'Anne',
          pets: [
            { name: 'Wishbone' },
          ],
        },
      ]);
    });
  });
});
