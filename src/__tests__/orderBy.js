import gql from 'graphql-tag';

import testGraphql from '../testUtils/expect-graphql';
import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';

const typeDefs = gql`
  type Product @autocrud(table: "products") {
    id: ID!
    name: String!
    relatedProducts: [Product!]! @relation(through: { table: "relatedProducts", near: "aID", far: "bID" })
    ratings: [Rating!]! @relation(to: "productID")
  }

  type Customer @autocrud(table: "customers") {
    id: ID!
    name: String!
    ratings: [Rating!]! @relation(to: "customerID")
  }

  type Rating @autocrud(table: "ratings", pivotInput: "RatingInput") {
    product: Product! @relation(from: "productID")
    customer: Customer! @relation(from: "customerID")
    rating: Int!
  }

  input RatingInput {
    id: ID!
    rating: Int!
  }
`;

describe('orderBy', () => {
  describe('createOrderByType', () => {
    let tester;
    beforeEach(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          relationTypeDefs,
          typeDefs,
        ],
        directives: {
          ...AutoCRUDDirectiveStages,
          ...RelationDirectiveStages,
        },
      });
    });

    testGraphql.schema(() => tester.schema)
      .shouldHaveTypes([
        'Customer',
        'Product',
        'Rating',
      ])
      .shouldHaveInputs([
        'CustomerOrderBy',
        'ProductOrderBy',
        'RatingOrderBy',
      ])
      .shouldHaveEnums([
        'CustomerOrderByFields',
      ])
      .shouldHaveQueries({
        customer: ['Customer', { id: 'ID!' }],
        allCustomers: ['[Customer!]!', { filter: 'CustomerFilter', orderBy: '[CustomerOrderBy]', limit: 'Int', offset: 'Int' }],
      });

    testGraphql.object(() => tester.schema.getType('CustomerOrderBy'))
      .shouldOnlyHaveFields({
        desc: 'Boolean',
        field: 'CustomerOrderByFields',
      });

    testGraphql.object(() => tester.schema.getType('CustomerOrderByFields'))
      .shouldOnlyHaveEnumKeys([
        'name',
        'ratings_rating',
        'ratings_product_name',
      ]);
  });

  describe('applyKnexOrderBy', () => {
    let tester;

    beforeAll(async () => {
      tester = await new AutoCRUDTester({
        typeDefs: [
          autoCRUDTypeDefs,
          relationTypeDefs,
          typeDefs,
        ],
        directives: {
          ...AutoCRUDDirectiveStages,
          ...RelationDirectiveStages,
        },
      });

      await tester.knex.schema.createTable('products', (table) => {
        table.uuid('id').unique();
        table.text('name');
      });

      await tester.knex.schema.createTable('relatedProducts', (table) => {
        table.uuid('aID');
        table.uuid('bID');
      });

      await tester.knex.schema.createTable('customers', (table) => {
        table.uuid('id').unique();
        table.text('name');
      });

      await tester.knex.schema.createTable('ratings', (table) => {
        table.uuid('productID');
        table.uuid('customerID');
        table.specificType('rating', 'smallint');
      });

      await tester.knex('products').insert([
        { id: tester.uuid('prod1'), name: 'Computer' },
        { id: tester.uuid('prod2'), name: 'Car' },
        { id: tester.uuid('prod3'), name: 'Monitor' },
      ]);

      await tester.knex('relatedProducts').insert([
        // Inverting it here is a sloppy way to do the relationship, but good
        // enough for the test
        { aID: tester.uuid('prod1'), bID: tester.uuid('prod3') },
        { aID: tester.uuid('prod3'), bID: tester.uuid('prod1') },
      ]);

      await tester.knex('customers').insert([
        { id: tester.uuid('user1'), name: 'John' },
        { id: tester.uuid('user2'), name: 'Chris' },
        { id: tester.uuid('user3'), name: 'Will' },
      ]);

      await tester.knex('ratings').insert([
        { productID: tester.uuid('prod1'), customerID: tester.uuid('user1'), rating: 4 },
        { productID: tester.uuid('prod1'), customerID: tester.uuid('user2'), rating: 5 },
        { productID: tester.uuid('prod2'), customerID: tester.uuid('user2'), rating: 3 },
      ]);
    });

    test('should sort base fields ascending', async () => {
      const graphqlResult = await tester.query(`
        query {
          allCustomers(orderBy: [
            { field: name }
          ]) {
            name
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allCustomers: [
            { name: 'Chris' },
            { name: 'John' },
            { name: 'Will' },
          ],
        },
      });
    });

    test('should sort base fields descending', async () => {
      const graphqlResult = await tester.query(`
        query {
          allCustomers(orderBy: [
            { field: name, desc: true },
          ]) {
            name
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allCustomers: [
            { name: 'Will' },
            { name: 'John' },
            { name: 'Chris' },
          ],
        },
      });
    });

    test('should sort through relationships ascending', async () => {
      const graphqlResult = await tester.query(`
        query {
          allProducts(orderBy: [
            { field: relatedProducts_name, desc: false },
          ]) {
            name
            relatedProducts { name }
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allProducts: [
            { name: 'Car', relatedProducts: [] },
            { name: 'Monitor', relatedProducts: [{ name: 'Computer' }] },
            { name: 'Computer', relatedProducts: [{ name: 'Monitor' }] },
          ],
        },
      });
    });

    test('should sort through relationships descending', async () => {
      const graphqlResult = await tester.query(`
        query {
          allProducts(orderBy: [
            { field: relatedProducts_name, desc: true },
          ]) {
            name
            relatedProducts { name }
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allProducts: [
            { name: 'Computer', relatedProducts: [{ name: 'Monitor' }] },
            { name: 'Monitor', relatedProducts: [{ name: 'Computer' }] },
            { name: 'Car', relatedProducts: [] },
          ],
        },
      });
    });

    test('should sort pivot relationships ascending', async () => {
      const graphqlResult = await tester.query(`
        query {
          allCustomers(orderBy: [
            { field: ratings_product_name, desc: false },
          ]) {
            name
            ratings { product { name } }
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allCustomers: [
            { name: 'Will', ratings: [] },
            {
              name: 'Chris',
              // Order of the ratings themselves will not be sorted
              ratings: [
                { product: { name: 'Computer' } },
                { product: { name: 'Car' } },
              ],
            },
            { name: 'John', ratings: [{ product: { name: 'Computer' } }] },
          ],
        },
      });
    });

    test('should sort pivot relationships descending', async () => {
      const graphqlResult = await tester.query(`
        query {
          allCustomers(orderBy: [
            { field: ratings_product_name, desc: true },
          ]) {
            name
            ratings { product { name } }
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allCustomers: [
            { name: 'John', ratings: [{ product: { name: 'Computer' } }] },
            {
              name: 'Chris',
              // Order of the ratings themselves will not be sorted
              ratings: [
                { product: { name: 'Computer' } },
                { product: { name: 'Car' } },
              ],
            },
            { name: 'Will', ratings: [] },
          ],
        },
      });
    });

    test('should handle multiple sorts', async () => {
      const graphqlResult = await tester.query(`
        query {
          allRatings(orderBy: [
            { field: customer_name, desc: false },
            { field: rating, desc: false },
          ]) {
            customer { name }
            rating
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allRatings: [
            { rating: 3, customer: { name: 'Chris' } },
            { rating: 5, customer: { name: 'Chris' } },
            { rating: 4, customer: { name: 'John' } },
          ],
        },
      });
    });

    test('should handle multiple sorts with different directions', async () => {
      const graphqlResult = await tester.query(`
        query {
          allRatings(orderBy: [
            { field: customer_name, desc: false },
            { field: rating, desc: true },
          ]) {
            customer { name }
            rating
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allRatings: [
            { rating: 5, customer: { name: 'Chris' } },
            { rating: 3, customer: { name: 'Chris' } },
            { rating: 4, customer: { name: 'John' } },
          ],
        },
      });
    });

    test('should handle orderBy with a limit and offset', async () => {
      const graphqlResult = await tester.query(`
        query {
          allCustomers(orderBy: [{ field: name }], limit: 1, offset: 1) {
            name
          }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();
      expect(graphqlResult).toEqual({
        data: {
          allCustomers: [
            // { name: 'Chris' }, // Hidden by offset
            { name: 'John' },
            // { name: 'Will' }, // Hidden by limit
          ],
        },
      });
    });
  });
});
