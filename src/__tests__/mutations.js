import { v4 as uuidv4 } from 'uuid';
import getKnexInstance from '../testUtils/knex';
import { knexUpdatePivots } from '../mutations/knexQueries';

const uuids = ['a', 'b', 'c', 'x', 'y', 'z'].reduce((acc, next) => {
  acc[next] = uuidv4();
  return acc;
}, {});

const initialData = [
  { leftID: uuids.a, rightID: uuids.x, rating: 3 },
  { leftID: uuids.a, rightID: uuids.y, rating: 4 },
  { leftID: uuids.b, rightID: uuids.y, rating: 3 },
];

const sortableName = (v) => `${v.leftID}-${v.rightID}-${v.rating}`;
const sortRows = (a, b) => sortableName(a).localeCompare(sortableName(b));

describe('knexUpdatePivots', () => {
  let knex;
  let destroyKnex;

  beforeAll(async () => {
    const knexInstance = await getKnexInstance();
    knex = knexInstance.knex;
    destroyKnex = knexInstance.destroyKnex;

    await knex.schema.createTable('knexUpdatePivots', (table) => {
      table.uuid('leftID');
      table.uuid('rightID');
      table.specificType('rating', 'smallint');
    });
  });

  afterAll(() => destroyKnex());

  beforeEach(async () => {
    await knex('knexUpdatePivots').truncate();
    await knex('knexUpdatePivots').insert(initialData);
  });

  test('should insert rows', async () => {
    const changeData = initialData
      .filter((v) => v.leftID === uuids.a)
      .concat({ leftID: uuids.a, rightID: uuids.z, rating: 1 });

    await Promise.all(knexUpdatePivots(knex, {
      upsert: { knexUpdatePivots: changeData },
      pivotKeys: { knexUpdatePivots: { near: 'leftID', far: 'rightID' } },
    }, uuids.a));

    const newData = await knex('knexUpdatePivots');

    expect(newData.sort(sortRows)).toEqual(initialData
      .filter((v) => v.leftID !== uuids.a)
      .concat(changeData)
      .sort(sortRows));
  });

  test('should update rows', async () => {
    const changeData = initialData.filter((v) => v.leftID === uuids.a);
    changeData[1] = { ...changeData[1], rating: 1 };

    await Promise.all(knexUpdatePivots(knex, {
      upsert: { knexUpdatePivots: changeData },
      pivotKeys: { knexUpdatePivots: { near: 'leftID', far: 'rightID' } },
    }, uuids.a));

    const newData = await knex('knexUpdatePivots');

    expect(newData.sort(sortRows)).toEqual(initialData
      .filter((v) => v.leftID !== uuids.a)
      .concat(changeData)
      .sort(sortRows));
  });

  test('should delete rows', async () => {
    const changeData = [{ leftID: uuids.a, rightID: uuids.z, rating: 1 }];

    await Promise.all(knexUpdatePivots(knex, {
      upsert: { knexUpdatePivots: changeData },
      pivotKeys: { knexUpdatePivots: { near: 'leftID', far: 'rightID' } },
    }, uuids.a));

    const newData = await knex('knexUpdatePivots');

    expect(newData.sort(sortRows)).toEqual(initialData
      .filter((v) => v.leftID !== uuids.a)
      .concat(changeData)
      .sort(sortRows));
  });

  test('should insert, update, and delete simultaneously', async () => {
    const changeData = initialData
      .filter((v) => v.leftID === uuids.a)
      .slice(1) // Delete the first one
      .concat({ leftID: uuids.a, rightID: uuids.z, rating: 1 }); // Add one

    changeData[0] = { ...changeData[0], rating: 1 }; // Change what was the 2nd one

    await Promise.all(knexUpdatePivots(knex, {
      upsert: { knexUpdatePivots: changeData },
      pivotKeys: { knexUpdatePivots: { near: 'leftID', far: 'rightID' } },
    }, uuids.a));

    const newData = await knex('knexUpdatePivots');

    expect(newData.sort(sortRows)).toEqual(initialData
      .filter((v) => v.leftID !== uuids.a)
      .concat(changeData)
      .sort(sortRows));
  });
});
