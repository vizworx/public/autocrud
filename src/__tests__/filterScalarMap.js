import gql from 'graphql-tag';
import { TimestampResolver } from 'graphql-scalars';
import { v4 as uuidv4 } from 'uuid';

import testGraphql from '../testUtils/expect-graphql';
import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';

const typeDefs = gql`
  type Test @autocrud(table: "tests") {
    id: ID!
    name: String!
    timestamp: Timestamp!
  }

  scalar Timestamp
`;

const resolvers = {
  Timestamp: TimestampResolver,
};

const getTester = (options) => new AutoCRUDTester({
  typeDefs: [
    autoCRUDTypeDefs,
    typeDefs,
  ],
  resolvers,
  directives: { ...AutoCRUDDirectiveStages },
  options,
});

const rows = [
  { id: uuidv4(), name: 'New Years', timestamp: new Date('January 1, 2000').getTime() / 1000 },
  { id: uuidv4(), name: 'Christmas', timestamp: new Date('December 25, 2000').getTime() / 1000 },
];

describe('options.filterScalarMap', () => {
  let tester;
  describe('No mapping', () => {
    beforeAll(async () => {
      tester = await getTester();
      await tester.resetDB();

      const { knex } = tester;

      await knex.schema.createTable('tests', (t) => {
        t.uuid('id').unique();
        t.string('name');
        t.integer('timestamp');
      });

      await knex('tests').insert(rows);
    });

    testGraphql.schema(() => tester.schema)
      .shouldHaveTypes([
        'Test',
      ])
      .shouldHaveInputs([
        'TestParams',
        'TestFilter',
      ])
      .shouldHaveQueries({
        test: ['Test', { id: 'ID!' }],
        allTests: ['[Test!]!', {
          filter: 'TestFilter',
          orderBy: '[TestOrderBy]',
          limit: 'Int',
          offset: 'Int',
        }],
      })
      .shouldHaveMutations({
        createTest: ['Test!', { test: 'TestParams!' }],
        updateTest: ['Test!', { id: 'ID!', test: 'TestParams!' }],
        deleteTest: ['Boolean', { id: 'ID!' }],
      });

    testGraphql.object(() => tester.schema.getType('TestFilter'))
      .shouldOnlyHaveFields({
        AND: '[TestFilter!]',
        OR: '[TestFilter!]',
        id_in: '[ID!]',
        id: 'ID',
        name: 'String',
        name_contains: 'String',
        name_startsWith: 'String',
        name_endsWith: 'String',
        name_in: '[String!]',
        timestamp: 'Timestamp',
      });

    test('should query successfully', async () => {
      const graphqlResult = await tester.query(`
        query {
          allTests { id name timestamp }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          allTests: rows,
        },
      });
    });

    test('should filter exact successfully', async () => {
      const graphqlResult = await tester.query(
        `
          query ($filter: TestFilter!) {
            allTests(filter: $filter) { id name timestamp }
          }
        `,
        { filter: { timestamp: rows[0].timestamp } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          allTests: [rows[0]],
        },
      });
    });
  });

  describe('Mapping Timestamp scalar', () => {
    beforeAll(async () => {
      tester = await getTester({ filterScalarMap: { Timestamp: 'Int' } });
      await tester.resetDB();

      const { knex } = tester;

      await knex.schema.createTable('tests', (t) => {
        t.uuid('id').unique();
        t.string('name');
        t.integer('timestamp');
      });

      await knex('tests').insert(rows);
    });

    testGraphql.schema(() => tester.schema)
      .shouldHaveTypes([
        'Test',
      ])
      .shouldHaveInputs([
        'TestParams',
        'TestFilter',
      ])
      .shouldHaveQueries({
        test: ['Test', { id: 'ID!' }],
        allTests: ['[Test!]!', {
          filter: 'TestFilter',
          orderBy: '[TestOrderBy]',
          limit: 'Int',
          offset: 'Int',
        }],
      })
      .shouldHaveMutations({
        createTest: ['Test!', { test: 'TestParams!' }],
        updateTest: ['Test!', { id: 'ID!', test: 'TestParams!' }],
        deleteTest: ['Boolean', { id: 'ID!' }],
      });

    testGraphql.object(() => tester.schema.getType('TestFilter'))
      .shouldOnlyHaveFields({
        AND: '[TestFilter!]',
        OR: '[TestFilter!]',
        id_in: '[ID!]',
        id: 'ID',
        name: 'String',
        name_contains: 'String',
        name_startsWith: 'String',
        name_endsWith: 'String',
        name_in: '[String!]',
        timestamp: 'Timestamp',
        timestamp_lt: 'Timestamp',
        timestamp_lte: 'Timestamp',
        timestamp_gt: 'Timestamp',
        timestamp_gte: 'Timestamp',
        timestamp_in: '[Timestamp!]',
      });

    test('should query successfully', async () => {
      const graphqlResult = await tester.query(`
        query {
          allTests { id name timestamp }
        }
      `);

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          allTests: rows,
        },
      });
    });

    test('should filter exact successfully', async () => {
      const graphqlResult = await tester.query(
        `
          query ($filter: TestFilter!) {
            allTests(filter: $filter) { id name timestamp }
          }
        `,
        { filter: { timestamp: rows[0].timestamp } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          allTests: [rows[0]],
        },
      });
    });

    test('should filter comparisons successfully', async () => {
      const graphqlResult = await tester.query(
        `
          query ($filter: TestFilter!) {
            allTests(filter: $filter) { id name timestamp }
          }
        `,
        { filter: { timestamp_gt: rows[0].timestamp } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult).toEqual({
        data: {
          allTests: [rows[1]],
        },
      });
    });
  });
});
