import gql from 'graphql-tag';

import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';

const typeDefs = gql`
  type Series @autocrud(table: "series") {
    id: ID!
    title: String!
    books: [Book!]! @relation(to: "seriesID")
  }

  type Author @autocrud(table: "authors") {
    id: ID!
    name: String!
    books: [Book!]! @relation(to: "authorID")
  }

  type Book @autocrud(table: "books") {
    id: ID!
    title: String!
    publishedYear: Int!
    seriesID: ID
    series: Series @relation(from: "seriesID")
    authorID: ID!
    author: Author! @relation(from: "authorID")
  }

  type Tag @autocrud(table: "tags") {
    id: ID!
    name: String!
    childOfName: String
    childOf: Tag @relation(from: "childOfName", to: "name")
  }
`;

const populateDatabase = async (tester) => {
  const { knex } = tester;

  await knex.schema.createTable('series', (t) => {
    t.uuid('id').unique();
    t.string('title');
  });

  await knex.schema.createTable('authors', (t) => {
    t.uuid('id').unique();
    t.string('name');
  });

  await knex.schema.createTable('books', (t) => {
    t.uuid('id').unique();
    t.string('title');
    t.smallint('publishedYear');
    t.uuid('seriesID').references('series.id').nullable();
    t.uuid('authorID').references('authors.id');
  });

  await knex.schema.createTable('tags', (t) => {
    t.string('name').unique();
    t.string('childOfName').references('tags.name').nullable();
  });

  await knex('authors').insert([
    { id: tester.uuid('author-1'), name: 'John Ronald Reuel Tolkien' },
    { id: tester.uuid('author-2'), name: 'Douglas Noel Adams' },
    { id: tester.uuid('author-3'), name: 'George Raymond Richard Martin' },
    { id: tester.uuid('author-4'), name: 'Eoin Colfer' },
  ]);

  await knex('series').insert([
    { id: tester.uuid('series-1'), title: 'The Lord Of The Rings' },
    { id: tester.uuid('series-2'), title: 'The Hitchhiker\'s Guide To The Galaxy' },
    { id: tester.uuid('series-3'), title: 'Dirk Gently' },
    { id: tester.uuid('series-4'), title: 'A Song Of Ice And Fire' },
  ]);

  await knex('books').insert([
    {
      id: tester.uuid('book-1'),
      title: 'The Fellowship Of The Ring',
      authorID: tester.uuid('author-1'),
      seriesID: tester.uuid('series-1'),
      publishedYear: 1954,
    },
    {
      id: tester.uuid('book-2'),
      title: 'The Two Towers',
      authorID: tester.uuid('author-1'),
      seriesID: tester.uuid('series-1'),
      publishedYear: 1954,
    },
    {
      id: tester.uuid('book-3'),
      title: 'The Return Of The King',
      authorID: tester.uuid('author-1'),
      seriesID: tester.uuid('series-1'),
      publishedYear: 1955,
    },

    {
      id: tester.uuid('book-4'),
      title: 'The Hitchhiker\'s Guide To The Galaxy',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-2'),
      publishedYear: 1979,
    },
    {
      id: tester.uuid('book-5'),
      title: 'The Restaurant At The End Of The Universe',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-2'),
      publishedYear: 1980,
    },
    {
      id: tester.uuid('book-6'),
      title: 'Life, The Universe, And Everything',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-2'),
      publishedYear: 1982,
    },
    {
      id: tester.uuid('book-7'),
      title: 'So Long And Thanks For All The Fish',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-2'),
      publishedYear: 1984,
    },
    {
      id: tester.uuid('book-8'),
      title: 'Mostly Harmless',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-2'),
      publishedYear: 1992,
    },
    {
      id: tester.uuid('book-9'),
      title: 'And Another Thing',
      authorID: tester.uuid('author-4'),
      seriesID: tester.uuid('series-2'),
      publishedYear: 2009,
    },

    {
      id: tester.uuid('book-10'),
      title: 'Dirk Gently\'s Holistic Detective Agency',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 1987,
    },
    {
      id: tester.uuid('book-11'),
      title: 'The Long Dark Tea-Time Of The Soul',
      authorID: tester.uuid('author-2'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 1988,
    },

    {
      id: tester.uuid('book-12'),
      title: 'A Game of Thrones',
      authorID: tester.uuid('author-3'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 1996,
    },
    {
      id: tester.uuid('book-13'),
      title: 'A Clash of Kings',
      authorID: tester.uuid('author-3'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 1999,
    },
    {
      id: tester.uuid('book-14'),
      title: 'A Storm of Swords',
      authorID: tester.uuid('author-3'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 2000,
    },
    {
      id: tester.uuid('book-15'),
      title: 'A Feast for Crows',
      authorID: tester.uuid('author-3'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 2005,
    },
    {
      id: tester.uuid('book-16'),
      title: 'A Dance with Dragons',
      authorID: tester.uuid('author-3'),
      seriesID: tester.uuid('series-3'),
      publishedYear: 2011,
    },
  ]);

  await knex('tags').insert([
    { name: '1', childOfName: null },
    { name: '1.1', childOfName: '1' },
    { name: '1.2', childOfName: '1' },
    { name: '1.1.1', childOfName: '1.1' },
    { name: '1.1.2', childOfName: '1.1' },
    { name: '1.2.1', childOfName: '1.2' },
    { name: '1.2.2', childOfName: '1.2' },
    { name: '1.1.1.1', childOfName: '1.1.1' },
    { name: '1.1.1.2', childOfName: '1.1.1' },
    { name: '1.1.2.1', childOfName: '1.1.2' },
    { name: '1.1.2.2', childOfName: '1.1.2' },
    { name: '1.2.1.1', childOfName: '1.2.1' },
    { name: '1.2.1.2', childOfName: '1.2.1' },
    { name: '1.2.2.1', childOfName: '1.2.2' },
    { name: '1.2.2.2', childOfName: '1.2.2' },
    { name: '2', childOfName: null },
    { name: '2.1', childOfName: '2' },
    { name: '2.2', childOfName: '2' },
    { name: '2.1.1', childOfName: '2.1' },
    { name: '2.1.2', childOfName: '2.1' },
    { name: '2.2.1', childOfName: '2.2' },
    { name: '2.2.2', childOfName: '2.2' },
    { name: '3', childOfName: null },
    { name: '4', childOfName: null },
  ]);
};

describe('filters', () => {
  let tester;

  const allBooksWithFilter = async (filter) => tester.query(
    `query ($filter: BookFilter) {
      allBooks(filter: $filter) {
        title
      }
    }`,
    { filter },
  );

  const allAuthorsWithFilter = async (filter) => tester.query(
    `query ($filter: AuthorFilter) {
      allAuthors(filter: $filter) {
        name
      }
    }`,
    { filter },
  );

  const allTagsWithFilter = async (filter) => tester.query(
    `query ($filter: TagFilter) {
      allTags(filter: $filter) {
        name
      }
    }`,
    { filter },
  );

  beforeAll(async () => {
    tester = await new AutoCRUDTester({
      typeDefs: [
        autoCRUDTypeDefs,
        relationTypeDefs,
        typeDefs,
      ],
      // resolvers,
      directives: { ...AutoCRUDDirectiveStages, ...RelationDirectiveStages },
    });

    await populateDatabase(tester);
  });

  test('scalar', async () => {
    const graphqlResult = await allBooksWithFilter({
      title_contains: 'Galaxy',
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(1);
  });

  test('multiple scalars - OR', async () => {
    const graphqlResult = await allBooksWithFilter({
      OR: [
        { title_contains: 'Galaxy' },
        { title_contains: 'Dragons' },
      ],
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(2);
  });

  test('multiple scalars - implicit AND', async () => {
    const graphqlResult = await allBooksWithFilter({
      title_contains: 'ing',
      authorID: tester.uuid('author-1'),
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(2);
  });

  test('multiple scalars - explicit AND', async () => {
    const graphqlResult = await allBooksWithFilter({
      AND: [
        { title_contains: 'ing' },
        { authorID: tester.uuid('author-1') },
      ],
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(2);
  });

  test('single nested', async () => {
    const graphqlResult = await allBooksWithFilter({
      author: {
        name_contains: 'Martin',
      },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(5);
  });

  test('single nested deep', async () => {
    const graphqlResult = await allAuthorsWithFilter({
      books_some: {
        series: {
          title_contains: 'Galaxy',
        },
      },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allAuthors).toHaveLength(2);
  });

  test('multiple nested - OR', async () => {
    const graphqlResult = await allBooksWithFilter({
      OR: [
        { author: { name_contains: 'Martin' } },
        { series: { title_contains: 'Rings' } },
      ],
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(8);
  });

  test('multiple nested - implicit AND', async () => {
    const graphqlResult = await allBooksWithFilter({
      author: { name_contains: 'Adams' },
      series: { title_contains: 'Galaxy' },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(5);
  });

  test('multiple nested - explicit AND', async () => {
    const graphqlResult = await allBooksWithFilter({
      AND: [
        { author: { name_contains: 'Adams' } },
        { series: { title_contains: 'Galaxy' } },
      ],
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(5);
  });

  test('mixed - implicit AND', async () => {
    const graphqlResult = await allBooksWithFilter({
      title_contains: 'ing',
      author: { name_contains: 'Adams' },
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allBooks).toHaveLength(1);
  });

  test('chained - OR - 2 levels', async () => {
    const graphqlResult = await allTagsWithFilter({
      OR: [
        { childOfName: '1' },
        { childOf: { childOfName: '1' } },
      ],
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allTags).toHaveLength(6);
  });

  test('chained - OR - 3 levels', async () => {
    const graphqlResult = await allTagsWithFilter({
      OR: [
        { childOfName: '1' },
        { childOf: { childOfName: '1' } },
        { childOf: { childOf: { childOfName: '1' } } },
      ],
    });

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(graphqlResult.data.allTags).toHaveLength(14);
  });

  xdescribe('need tests', () => {
    test('multiple nested - AND > OR', async () => {});
    test('multiple nested - OR > AND', async () => {});
    test('plural types - every', async () => {});
    test('plural types - some', async () => {});
    test('plural types - none', async () => {});
    test('different relation types', async () => {});
    test('interfaces', async () => {});
    test('interfaces - nesting', async () => {});
    test('interfaces - chaining', async () => {});
  });
});
