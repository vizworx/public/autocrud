import gql from 'graphql-tag';

import AutoCRUDTester from '../testUtils/AutoCRUDTester';

import { directiveStages as AutoCRUDDirectiveStages, typeDefs as autoCRUDTypeDefs } from '../AutoCRUDDirective';
import { directiveStages as RelationDirectiveStages, typeDefs as relationTypeDefs } from '../RelationDirective';

const typeDefs = gql`
  type Author @autocrud(table: "authors") {
    id: ID!
    name: String!
    books: [Book!]!
  }

  type Book implements WrittenMaterial @autocrud(table: "books") {
    id: ID!
    title: String!
    authorID: ID!
    author: Author! @relation(from: "authorID")
  }

  type Series implements WrittenMaterial @autocrud(table: "series") {
    id: ID!
    title: String!
    authorID: ID!
    author: Author! @relation(from: "authorID")
  }

  interface WrittenMaterial @autocrud {
    title: String!
    authorID: ID!
    author: Author! @relation(from: "authorID")
  }
`;

const resolvers = {
  WrittenMaterial: {
    __resolveType: (obj) => obj.autocrud_model,
  },
};

const populateDatabase = async (tester) => {
  await tester.resetDB();
  const { knex } = tester;

  await knex.schema.createTable('authors', (t) => {
    t.uuid('id').unique();
    t.string('name');
  });

  await knex.schema.createTable('books', (t) => {
    t.uuid('id').unique();
    t.string('title');
    t.uuid('authorID').references('authors.id');
  });

  await knex.schema.createTable('series', (t) => {
    t.uuid('id').unique();
    t.string('title');
    t.uuid('authorID').references('authors.id');
  });

  await knex('authors').insert([
    { id: tester.uuid('authors-1'), name: 'Tolkien' },
    { id: tester.uuid('authors-2'), name: 'Adams' },
    { id: tester.uuid('authors-3'), name: 'Martin' },
  ]);

  await knex('books').insert([
    { id: tester.uuid('books-1'), title: 'The Fellowship Of The Ring', authorID: tester.uuid('authors-1') },
    { id: tester.uuid('books-2'), title: 'The Two Towers', authorID: tester.uuid('authors-1') },
    { id: tester.uuid('books-3'), title: 'The Hitchhiker\'s Guide To The Galaxy', authorID: tester.uuid('authors-2') },
    { id: tester.uuid('books-4'), title: 'The Restaurant At The End Of The Universe', authorID: tester.uuid('authors-2') },
    { id: tester.uuid('books-5'), title: 'A Storm Of Swords', authorID: tester.uuid('authors-3') },
    { id: tester.uuid('books-6'), title: 'The Winds Of Winter', authorID: tester.uuid('authors-3') },
  ]);

  await knex('series').insert([
    { id: tester.uuid('series-1'), title: 'The Lord Of The Rings', authorID: tester.uuid('authors-1') },
    { id: tester.uuid('series-2'), title: 'A Game Of Thrones', authorID: tester.uuid('authors-3') },
  ]);
};

const allHooks = [
  'preOperation',
  'preQuery',
  'preMutation',
  'preCommit',
  'postCommit',
  'prePublish',
  'postPublish',
  'postMutation',
  'postQuery',
  'postOperation',
];

describe('hooks', () => {
  let tester;
  let log = [];

  const getTester = async (other = {}) => {
    tester = await new AutoCRUDTester({
      typeDefs: [
        autoCRUDTypeDefs,
        relationTypeDefs,
        typeDefs,
      ],
      resolvers,
      directives: {
        ...AutoCRUDDirectiveStages,
        ...RelationDirectiveStages,
      },
      ...other,
    });

    await populateDatabase(tester);
    tester.knex.on('query', ({ sql, bindings }) => log.push({ sql, bindings }));
    log.splice(0); // Delete everything in the array, but keep the reference
  };

  beforeEach(() => { log = []; });

  describe('global hooks should fire in the correct order', () => {
    beforeAll(async () => {
      const hooks = allHooks.reduce((acc, cur) => {
        acc[cur] = () => log.push(cur);

        return acc;
      }, {});

      const pubsub = {
        publish: () => log.push('publish'),
      };

      await getTester({ hooks, pubsub });
    });

    test('all query', async () => {
      const graphqlResult = await tester.query(
        `query {
          allBooks {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preQuery',
        (v) => v.sql?.match(/select.+books/),
        'postQuery',
        'postOperation',
      ]);
    });

    test('single query', async () => {
      const graphqlResult = await tester.query(
        `query ($id: ID!) {
          book(id: $id) {
            title
          }
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preQuery',
        (v) => v.sql?.match(/select.+books/),
        'postQuery',
        'postOperation',
      ]);
    });

    test('create', async () => {
      const graphqlResult = await tester.query(
        `mutation ($book: BookParams!) {
          createBook(book: $book) {
            title
          }
        }`,
        { book: { title: 'The Hobbit', authorID: tester.uuid('authors-1') } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preMutation',
        (v) => v.sql?.match('BEGIN;'),
        'preCommit',
        (v) => v.sql?.match(/insert.+books/),
        'postCommit',
        (v) => v.sql?.match('COMMIT;'),
        'prePublish',
        'publish',
        'postPublish',
        'postMutation',
        'postOperation',
      ]);
    });

    test('update', async () => {
      const graphqlResult = await tester.query(
        `mutation ($id: ID!, $book: BookParams!) {
          updateBook(id: $id, book: $book) {
            title
          }
        }`,
        {
          id: tester.uuid('books-1'),
          book: {
            title: 'LOTR',
            authorID: tester.uuid('authors-1'), // Not changing
          },
        },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preMutation',
        (v) => v.sql?.match('BEGIN;'),
        (v) => v.sql?.match(/select.+books.+where.+id/),
        'preCommit',
        (v) => v.sql?.match(/select.+books.+where.+id/), // organizeFields
        (v) => v.sql?.match(/update.+books/),
        'postCommit',
        (v) => v.sql?.match('COMMIT;'),
        'prePublish',
        'publish',
        'postPublish',
        'postMutation',
        'postOperation',
      ]);
    });

    test('delete', async () => {
      const graphqlResult = await tester.query(
        `mutation ($id: ID!) {
          deleteBook(id: $id)
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preMutation',
        (v) => v.sql?.match('BEGIN;'),
        (v) => v.sql?.match(/select.+books.+where.+id/),
        'preCommit',
        (v) => v.sql?.match(/delete.+books/),
        'postCommit',
        (v) => v.sql?.match('COMMIT;'),
        'prePublish',
        'publish',
        'postPublish',
        'postMutation',
        'postOperation',
      ]);
    });

    test('interface query', async () => {
      const graphqlResult = await tester.query(
        `query {
          allWrittenMaterials {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preQuery',
        (v) => v.sql?.match(/select.+books/),
        'postQuery',
        'postOperation',
      ]);
    });
  });

  describe('type-specific hooks should fire in the correct order', () => {
    beforeAll(async () => {
      const hooks = allHooks.reduce((acc, cur) => {
        acc.Book[cur] = () => log.push(cur);

        // Just for the interface test
        acc.WrittenMaterial[cur] = () => log.push(cur);

        return acc;
      }, { Book: {}, WrittenMaterial: {} });

      const pubsub = {
        publish: () => log.push('publish'),
      };

      await getTester({ hooks, pubsub });
    });

    test('all query', async () => {
      const graphqlResult = await tester.query(
        `query {
          allBooks {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preQuery',
        (v) => v.sql?.match(/select.+books/),
        'postQuery',
        'postOperation',
      ]);
    });

    test('single query', async () => {
      const graphqlResult = await tester.query(
        `query ($id: ID!) {
          book(id: $id) {
            title
          }
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preQuery',
        (v) => v.sql?.match(/select.+books/),
        'postQuery',
        'postOperation',
      ]);
    });

    test('create', async () => {
      const graphqlResult = await tester.query(
        `mutation ($book: BookParams!) {
          createBook(book: $book) {
            title
          }
        }`,
        { book: { title: 'The Hobbit', authorID: tester.uuid('authors-1') } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preMutation',
        (v) => v.sql?.match('BEGIN;'),
        'preCommit',
        (v) => v.sql?.match(/insert.+books/),
        'postCommit',
        (v) => v.sql?.match('COMMIT;'),
        'prePublish',
        'publish',
        'postPublish',
        'postMutation',
        'postOperation',
      ]);
    });

    test('update', async () => {
      const graphqlResult = await tester.query(
        `mutation ($id: ID!, $book: BookParams!) {
          updateBook(id: $id, book: $book) {
            title
          }
        }`,
        {
          id: tester.uuid('books-1'),
          book: {
            title: 'LOTR',
            authorID: tester.uuid('authors-1'), // Not changing
          },
        },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preMutation',
        (v) => v.sql?.match(/select.+books.+where.+id/),
        'preCommit',
        (v) => v.sql?.match(/select.+books.+where.+id/), // organizeFields
        (v) => v.sql?.match(/update.+books/),
        'postCommit',
        'prePublish',
        'publish',
        'postPublish',
        'postMutation',
        'postOperation',
      ]);
    });

    test('delete', async () => {
      const graphqlResult = await tester.query(
        `mutation ($id: ID!) {
          deleteBook(id: $id)
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preMutation',
        (v) => v.sql?.match('BEGIN;'),
        (v) => v.sql?.match(/select.+books.+where.+id/),
        'preCommit',
        (v) => v.sql?.match(/delete.+books/),
        'postCommit',
        (v) => v.sql?.match('COMMIT;'),
        'prePublish',
        'publish',
        'postPublish',
        'postMutation',
        'postOperation',
      ]);
    });

    test('interface query', async () => {
      const graphqlResult = await tester.query(
        `query {
          allWrittenMaterials {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        'preOperation',
        'preQuery',
        (v) => v.sql?.match(/select.+books/),
        'postQuery',
        'postOperation',
      ]);
    });
  });

  describe('should provide the correct arguments to each hook point', () => {
    beforeEach(async () => {
      const hooks = allHooks.reduce((acc, cur) => {
        acc[cur] = (hookArgs, resolverArgs) => log.push([cur, hookArgs, Object.keys(resolverArgs)]);

        return acc;
      }, {});

      await getTester({ hooks, pubsub: { publish: () => {} } });
    });

    const getCalledHooks = () => log.reduce((acc, cur) => {
      if (!Array.isArray(cur)) { return acc; }

      const [hook, hookArgs, resolverArgs] = cur;

      if (acc[hook]) { return acc; }

      expect(resolverArgs).toEqual(['obj', 'args', 'context', 'info']);

      acc[hook] = hookArgs;

      return acc;
    }, {});

    test('all query', async () => {
      const graphqlResult = await tester.query(
        `query {
          allBooks {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const calledHooks = getCalledHooks();

      const expectedHookArgs = {
        type: 'Book',
        table: 'books',
      };

      expect(calledHooks.preOperation).toEqual(expectedHookArgs);

      expect(calledHooks.preQuery).toEqual(expectedHookArgs);

      expect(calledHooks.postQuery).toEqual({
        ...expectedHookArgs,
        result: expect.array(),
      });

      expect(calledHooks.postOperation).toEqual({
        ...expectedHookArgs,
        result: expect.array(),
      });
    });

    test('single query', async () => {
      const graphqlResult = await tester.query(
        `query ($id: ID!) {
          book(id: $id) {
            title
          }
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const calledHooks = getCalledHooks();

      const expectedHookArgs = {
        type: 'Book',
        table: 'books',
      };

      expect(calledHooks.preOperation).toEqual(expectedHookArgs);

      expect(calledHooks.preQuery).toEqual(expectedHookArgs);

      expect(calledHooks.postQuery).toEqual({
        ...expectedHookArgs,
        result: expect.any(Object),
      });

      expect(calledHooks.postOperation).toEqual({
        ...expectedHookArgs,
        result: expect.any(Object),
      });
    });

    test('create', async () => {
      const graphqlResult = await tester.query(
        `mutation ($book: BookParams!) {
          createBook(book: $book) {
            title
          }
        }`,
        { book: { title: 'The Hobbit', authorID: tester.uuid('authors-1') } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const expectedHookArgs = {
        action: 'create',
        id: expect.any(String),
        type: 'Book',
        table: 'books',
      };

      const calledHooks = getCalledHooks();

      expect(calledHooks.preOperation).toEqual(expectedHookArgs);
      expect(calledHooks.preMutation).toEqual(expectedHookArgs);

      expect(calledHooks.preCommit).toEqual({
        ...expectedHookArgs,
        transaction: expect.objectContaining({ isTransaction: true }),
      });

      expect(calledHooks.postCommit).toEqual({
        ...expectedHookArgs,
        transaction: expect.objectContaining({ isTransaction: true }),
      });

      expect(calledHooks.prePublish).toEqual(expectedHookArgs);
      expect(calledHooks.postPublish).toEqual(expectedHookArgs);
      expect(calledHooks.postMutation).toEqual(expectedHookArgs);
      expect(calledHooks.postOperation).toEqual(expectedHookArgs);
    });

    test('update', async () => {
      const graphqlResult = await tester.query(
        `mutation ($id: ID!, $book: BookParams!) {
          updateBook(id: $id, book: $book) {
            title
          }
        }`,
        {
          id: tester.uuid('books-1'),
          book: {
            title: 'LOTR',
            authorID: tester.uuid('authors-1'), // Not changing
          },
        },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const expectedHookArgs = {
        action: 'update',
        type: 'Book',
        table: 'books',
      };

      const calledHooks = getCalledHooks();

      expect(calledHooks.preOperation).toEqual(expectedHookArgs);
      expect(calledHooks.preMutation).toEqual(expectedHookArgs);

      expect(calledHooks.preCommit).toEqual({
        ...expectedHookArgs,
        prevState: {
          authorID: tester.uuid('authors-1'),
          id: tester.uuid('books-1'),
          title: 'The Fellowship Of The Ring',
        },
        transaction: expect.objectContaining({ isTransaction: true }),
      });

      expect(calledHooks.postCommit).toEqual({
        ...expectedHookArgs,
        prevState: {
          authorID: tester.uuid('authors-1'),
          id: tester.uuid('books-1'),
          title: 'The Fellowship Of The Ring',
        },
        transaction: expect.objectContaining({ isTransaction: true }),
      });

      expect(calledHooks.prePublish).toEqual(expectedHookArgs);
      expect(calledHooks.postPublish).toEqual(expectedHookArgs);
      expect(calledHooks.postMutation).toEqual(expectedHookArgs);
      expect(calledHooks.postOperation).toEqual(expectedHookArgs);
    });

    test('delete', async () => {
      const graphqlResult = await tester.query(
        `mutation ($id: ID!) {
          deleteBook(id: $id)
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const expectedHookArgs = {
        action: 'delete',
        type: 'Book',
        table: 'books',
      };

      const calledHooks = getCalledHooks();

      expect(calledHooks.preOperation).toEqual(expectedHookArgs);
      expect(calledHooks.preMutation).toEqual(expectedHookArgs);

      expect(calledHooks.preCommit).toEqual({
        ...expectedHookArgs,
        prevState: {
          authorID: tester.uuid('authors-1'),
          id: tester.uuid('books-1'),
          title: 'The Fellowship Of The Ring',
        },
        transaction: expect.objectContaining({ isTransaction: true }),
      });

      expect(calledHooks.postCommit).toEqual({
        ...expectedHookArgs,
        prevState: {
          authorID: tester.uuid('authors-1'),
          id: tester.uuid('books-1'),
          title: 'The Fellowship Of The Ring',
        },
        transaction: expect.objectContaining({ isTransaction: true }),
      });

      expect(calledHooks.prePublish).toEqual(expectedHookArgs);
      expect(calledHooks.postPublish).toEqual(expectedHookArgs);
      expect(calledHooks.postMutation).toEqual(expectedHookArgs);
      expect(calledHooks.postOperation).toEqual(expectedHookArgs);
    });

    test('interface query', async () => {
      const graphqlResult = await tester.query(
        `query {
          allWrittenMaterials {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const calledHooks = getCalledHooks();

      const expectedHookArgs = {
        type: 'WrittenMaterial',
        childTypes: ['Book', 'Series'],
      };

      expect(calledHooks.preOperation).toEqual(expectedHookArgs);

      expect(calledHooks.preQuery).toEqual(expectedHookArgs);

      expect(calledHooks.postQuery).toEqual({
        ...expectedHookArgs,
        result: expect.array(),
      });

      expect(calledHooks.postOperation).toEqual({
        ...expectedHookArgs,
        result: expect.array(),
      });
    });
  });

  describe('should allow hooks to modify the given args', () => {
    test('all query', async () => {
      const hooks = {
        preQuery: (hookArgs, resolverArgs) => {
          // eslint-disable-next-line no-param-reassign
          resolverArgs.args.orderBy = [{
            field: {
              name: 'title',
              fieldPath: [
                {
                  name: 'title',
                  onType: tester.schema.getTypeMap().Book,
                },
              ],
            },
          }];
        },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `query {
          allBooks {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const result = graphqlResult.data.allBooks;
      const sorted = result.slice().sort((a, b) => a.title - b.title);

      expect(sorted).toEqual(result);
    });

    test('single query', async () => {
      const hooks = {
        preQuery: (hookArgs, resolverArgs) => {
          // eslint-disable-next-line no-param-reassign
          resolverArgs.args.id = tester.uuid('books-2');
        },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `query ($id: ID!) {
          book(id: $id) {
            title
          }
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(graphqlResult.data.book.title).toEqual('The Two Towers');
    });

    test('create', async () => {
      const title = '2001: A Space Odyssey';

      const hooks = {
        preMutation: (hookArgs, resolverArgs) => {
          // eslint-disable-next-line no-param-reassign
          resolverArgs.args.book.title = title;
        },
        preCommit: (hookArgs, resolverArgs) => { log.push(resolverArgs.args.book.title); },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `mutation ($book: BookParams!) {
          createBook(book: $book) {
            title
          }
        }`,
        { book: { title: 'The Hobbit', authorID: tester.uuid('authors-1') } },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        title,
        (v) => (v.bindings?.indexOf(title) > -1),
      ]);

      const dbResult = await tester.knex('books').where({ title });

      expect(dbResult).toHaveLength(1);
    });

    test('update', async () => {
      const title = '2001: A Space Odyssey';

      const hooks = {
        preMutation: (hookArgs, resolverArgs) => {
          // eslint-disable-next-line no-param-reassign
          resolverArgs.args.book.title = title;
        },
        preCommit: (hookArgs, resolverArgs) => { log.push(resolverArgs.args.book.title); },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `mutation ($id: ID!, $book: BookParams!) {
          updateBook(id: $id, book: $book) {
            title
          }
        }`,
        {
          id: tester.uuid('books-1'),
          book: {
            title: 'LOTR',
            authorID: tester.uuid('authors-1'), // Not changing
          },
        },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        title,
        (v) => (v.bindings?.indexOf(title) > -1),
      ]);
    });

    test('delete', async () => {
      let id;

      const hooks = {
        preMutation: (hookArgs, resolverArgs) => {
          // eslint-disable-next-line no-param-reassign
          resolverArgs.args.id = id;
        },
        preCommit: (hookArgs, resolverArgs) => { log.push(resolverArgs.args.id); },
      };

      await getTester({ hooks });
      id = tester.uuid('books-2'); // We have to get the uuid after resetting the tester

      const graphqlResult = await tester.query(
        `mutation ($id: ID!) {
          deleteBook(id: $id)
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      expect(log).toContainInOrder([
        id,
        (v) => (v.bindings?.indexOf(id) > -1),
      ]);

      const dbResult = await tester.knex('books');

      const hasId1 = dbResult.some((row) => row.id === tester.uuid('books-1'));
      expect(hasId1).toEqual(true);

      const hasId2 = dbResult.some((row) => row.id === tester.uuid('books-2'));
      expect(hasId2).toEqual(false);
    });

    test('interface query', async () => {
      const hooks = {
        preQuery: (hookArgs, resolverArgs) => {
          // eslint-disable-next-line no-param-reassign
          resolverArgs.args.orderBy = [{
            field: {
              name: 'title',
              fieldPath: [
                {
                  name: 'title',
                  onType: tester.schema.getTypeMap().Book,
                },
              ],
            },
          }];
        },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `query {
          allWrittenMaterials {
            title
          }
        }`,
      );

      expect(graphqlResult).graphqlResultNotToHaveErrors();

      const result = graphqlResult.data.allWrittenMaterials;
      const sorted = result.slice().sort((a, b) => a.title - b.title);

      expect(sorted).toEqual(result);
    });
  });

  test('should fire multiple hooks sequentially', async () => {
    const hooks = {
      preQuery: () => log.push('global'),
      Book: {
        preQuery: [
          () => log.push(0),
          () => log.push(1),
          () => log.push(2),
        ],
      },
    };

    await getTester({ hooks });

    const graphqlResult = await tester.query(
      `query {
        allBooks {
          title
        }
      }`,
    );

    expect(graphqlResult).graphqlResultNotToHaveErrors();

    expect(log).toContainInOrder([
      'global',
      0,
      1,
      2,
    ]);
  });

  describe('should roll back transactions if a hook throws', () => {
    test('create', async () => {
      const hooks = {
        preCommit: () => { throw new Error('TEST ERROR'); },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `mutation ($book: BookParams!) {
          createBook(book: $book) {
            title
          }
        }`,
        { book: { title: 'SHOULD NOT EXIST', authorID: tester.uuid('authors-1') } },
      );

      expect(graphqlResult.errors?.[0]?.message).toEqual(expect.stringMatching('TEST ERROR'));

      const dbResult = await tester.knex('books').where({ title: 'SHOULD NOT EXIST' });

      expect(dbResult.length).toEqual(0);
    });

    test('update', async () => {
      const hooks = {
        preCommit: () => { throw new Error('TEST ERROR'); },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `mutation ($id: ID!, $book: BookParams!) {
          updateBook(id: $id, book: $book) {
            title
          }
        }`,
        {
          id: tester.uuid('books-1'),
          book: {
            title: 'LOTR',
            authorID: tester.uuid('authors-1'), // Not changing
          },
        },
      );

      expect(graphqlResult.errors?.[0]?.message).toEqual(expect.stringMatching('TEST ERROR'));

      const dbResult = await tester.knex('books').where({ id: tester.uuid('books-1') }).first();

      expect(dbResult.title).toEqual('The Fellowship Of The Ring');
    });

    test('delete', async () => {
      const hooks = {
        preCommit: () => { throw new Error('TEST ERROR'); },
      };

      await getTester({ hooks });

      const graphqlResult = await tester.query(
        `mutation ($id: ID!) {
          deleteBook(id: $id)
        }`,
        { id: tester.uuid('books-1') },
      );

      expect(graphqlResult.errors?.[0]?.message).toEqual(expect.stringMatching('TEST ERROR'));

      const dbResult = await tester.knex('books').where({ id: tester.uuid('books-1') }).first();

      expect(dbResult.title).toEqual('The Fellowship Of The Ring');
    });
  });
});
