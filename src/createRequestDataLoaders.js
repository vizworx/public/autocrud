import { getNamedType } from 'graphql';
import DataLoader from 'dataloader';
import { MissingQueriesDirective } from './errors';

const getLoaderName = (name, field, many) => `${name}_${field}${many ? '_many' : ''}`;

// Outer function is so that each request has its own set of data loaders. This
// prevents stale data from occurring across queries or permissions bleeding into
// other users queries.
export default (knex, schema) => {
  let tableLoaders = {};
  let modelLoaders = {};

  // Accept a table, field, and manyResults flag, and use that to generate the
  // knex query to return the appropriate records (either as a single object or
  // an array of objects, depending on manyResults)
  const getTableDataLoader = (table, { field = 'id', manyResults = false, builder = knex } = {}) => {
    const loaderName = getLoaderName(table, field, manyResults);

    // If we've already created the loader for this type, return it
    if (tableLoaders[loaderName]) { return tableLoaders[loaderName]; }

    tableLoaders[loaderName] = new DataLoader(async (ids) => builder(table)
      .select('*')
      .whereIn(field, ids)
      .then((rows) => {
        const initialAcc = {};

        // If we're expecting many results per lookup, fill initialAcc with arrays
        if (manyResults) {
          ids.forEach((id) => { initialAcc[id] = []; });
        }

        const rowLookup = rows.reduce((acc, cur) => {
          if (manyResults) {
            acc[cur[field]].push(cur);
          } else {
            acc[cur[field]] = cur;
          }

          return acc;
        }, initialAcc);

        return ids.map((id) => rowLookup[id] || new Error(`No result for ${id}`));
      }));

    return tableLoaders[loaderName];
  };

  // Accept the GraphQL type being queried and use the relation directive to
  // call getTableLoader
  const getModelDataLoader = (model, { field = 'id', manyResults = false, builder = knex } = {}) => {
    const modelType = typeof model === 'string'
      ? schema.getType(model)
      : model;

    const baseType = getNamedType(modelType.type || modelType);

    const loaderName = getLoaderName(baseType.name, field, manyResults);

    // If we've already created the loader for this type, return it
    if (modelLoaders[loaderName]) { return modelLoaders[loaderName]; }

    const table = baseType._queryInfo?.table; // eslint-disable-line no-underscore-dangle

    if (!table) { throw new MissingQueriesDirective(baseType); }

    modelLoaders[loaderName] = getTableDataLoader(table, { field, manyResults, builder });

    return modelLoaders[loaderName];
  };

  const clearAllLoaders = () => {
    tableLoaders = {};
    modelLoaders = {};
  };

  return { getModelDataLoader, getTableDataLoader, clearAllLoaders };
};
