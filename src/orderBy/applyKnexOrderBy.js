/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_filterInfo"] }] */

import { recurseToRelationQueries } from '../utilities';

const getOrderDescKeyword = (desc) => (desc ? 'DESC' : 'ASC');

const applyOrderBy = (knex, query, orderBy) => query
  .orderByRaw(orderBy.map(({ field, desc }) => {
    const orderWithNulls = ['postgresql', 'sqlite3'].includes(knex.context.client.config.client)
      ? `:column: ${getOrderDescKeyword(desc)} NULLS ${desc ? 'LAST' : 'FIRST'}`
      : `:column: IS NULL ${getOrderDescKeyword(!desc)}, :column: ${getOrderDescKeyword(desc)}`;

    return knex.raw(orderWithNulls, { column: knex.ref(field.name || field) });
  }).join(', '));

const applyOrderLimit = (knex, query, orderBy, desc) => applyOrderBy(
  knex,
  query,
  [{ field: orderBy, desc }],
).limit(1);

const recurseToNestedQueries = (
  knex,
  relationQueries,
  orderByName,
  desc,
  paths,
  prevAlias = 'base',
) => {
  // TODO: This may need to group parent relations together
  const [{ name: fieldName, column }, ...remaining] = paths;
  const queryAlias = `${prevAlias}_${fieldName}`;
  const relationSubquery = relationQueries[queryAlias];

  if (!relationSubquery) {
    return { [orderByName]: `${prevAlias}.${column || fieldName}` };
  }

  const allSubqueryWithSelect = relationSubquery.map(([tableAlias, subquery]) => {
    const selectBy = recurseToNestedQueries(
      knex,
      relationQueries,
      orderByName,
      desc,
      remaining,
      tableAlias,
    );

    return subquery.select(selectBy);
  });

  return {
    [orderByName]: applyOrderLimit(
      knex,
      allSubqueryWithSelect[0].union(allSubqueryWithSelect.slice(1)),
      orderByName,
      desc,
    ),
  };
};

const applyKnexOrderBy = (knex, schema, rawQuery, orderBy) => {
  // Begin by clustering the orderBy fields into their various (nested) tables
  const relationQueries = orderBy.reduce((acc, next) => recurseToRelationQueries(
    knex,
    acc,
    next.field.fieldPath,
  ), {});

  // Uncomment this to assist with understanding the relation queries that will be nested
  /*
  console.log(Object.fromEntries(
    Object.entries(relationQueries).map(([queryAlias, queries]) => {
      const sqlQueries = Object.fromEntries(queries
        .map(([tableAlias, query]) => ([tableAlias, query.toString()])));

      return [queryAlias, sqlQueries];
    }),
  ));
  */

  const orderSelectFields = orderBy.reduce((acc, { field: { fieldPath, name }, desc }) => ({
    ...acc,
    ...recurseToNestedQueries(knex, relationQueries, name, desc, fieldPath),
  }), {});

  return applyOrderBy(knex, rawQuery.select(orderSelectFields), orderBy);
};

export default applyKnexOrderBy;
