/* eslint no-underscore-dangle: ["error", { "allow": ["_queryInfo", "_dbColumn"] }] */

import {
  isLeafType,
  getNamedType,
  GraphQLInputObjectType,
  GraphQLUnionType,
  GraphQLEnumType,
  GraphQLID,
  GraphQLBoolean,
} from 'graphql';

const addFieldTo = (fields, name, type, defaultValue) => {
  fields[name] = { // eslint-disable-line no-param-reassign
    name,
    description: undefined,
    type,
    defaultValue,
    extensions: undefined,
    astNode: undefined,
  };
};

const buildOrderByFieldEnum = (modelType, depth = 0, previousType = null) => {
  const fieldEnum = [];

  const modelFieldTypes = Object.values(modelType.getFields())
    .reduce((acc, modelField) => {
      const modelFieldType = getNamedType(modelField.type);

      if (
        modelFieldType !== GraphQLID
        // Avoid walking back through a relationship
        && modelFieldType !== previousType
        // TODO: Unions add a bit of complexity, but we should try to support them
        && !(modelFieldType instanceof GraphQLUnionType)
      ) { acc.push(modelField); }

      return acc;
    }, []);

  // Loop through each field on the model and add the appropriate enums
  modelFieldTypes.forEach((modelField) => {
    const modelFieldType = getNamedType(modelField.type);

    if (isLeafType(modelFieldType)) {
      fieldEnum.push([{
        name: modelField.name,
        column: modelField._dbColumn?.name ?? modelField.name,
        onType: modelType,
      }]);
    } else if (depth < 1) {
      const newDepth = modelFieldType._queryInfo?.pivotInput
        ? depth // Don't increase the depth if going through a pivot type
        : depth + 1;

      buildOrderByFieldEnum(modelFieldType, newDepth, modelType).forEach((nested) => {
        // Change the name so that it is prefixed by this modelField's name
        fieldEnum.push([{ name: modelField.name, onType: modelType }].concat(nested));
      });
    }
  });

  return fieldEnum;
};

const createOrderByEnumType = (modelType, enums, schema) => {
  const orderByEnumName = `${modelType.name}OrderByFields`;
  let orderByEnumType = schema.getType(orderByEnumName);

  orderByEnumType = orderByEnumType || new GraphQLEnumType({
    name: orderByEnumName,
    values: Object.fromEntries(enums.map((fieldPath) => {
      // Name needs to be flattened: owners_name, tags_tag_name, etc
      // fieldPath: [{ name: 'owners', onType: Asset }, { name: 'name', onType: Label }]
      const joinedName = fieldPath.map(({ name }) => name).join('_');

      return [joinedName, { value: { fieldPath, name: joinedName } }];
    })),
  });

  schema.getTypeMap()[orderByEnumName] = orderByEnumType; // eslint-disable-line no-param-reassign

  return orderByEnumType;
};

const createOrderByType = (modelType, schema) => {
  const orderByName = `${modelType.name}OrderBy`;
  const fieldEnum = buildOrderByFieldEnum(modelType);

  // Short-circuit to avoid creating an (illegal) empty enum
  if (!fieldEnum?.length) { return null; }

  const orderByEnumType = createOrderByEnumType(modelType, fieldEnum, schema);

  const orderByType = new GraphQLInputObjectType({
    name: orderByName,
    fields: {},
  });

  const orderByFields = orderByType.getFields();
  addFieldTo(orderByFields, 'field', orderByEnumType);
  addFieldTo(orderByFields, 'desc', GraphQLBoolean, false);

  schema.getTypeMap()[orderByName] = orderByType; // eslint-disable-line no-param-reassign

  return orderByType;
};

export default createOrderByType;
