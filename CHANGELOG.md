# Changelog

## 3.5.0 (2021-11-05)

### Changes
- Adds `limit: Int, offset: Int` to any of the readAll queries
- Adds maping of scalars to base types for filtering

### Fixes
- Single relations are now set as required if the field is required
- Custom resolvers can now return an array of related IDs
- Type-specific hooks were never firing on `delete` mutations, because hooks were checked against the operation's return type and `delete` returns `Boolean`.

## 3.2.0 (2020-10-15)

### Changes
- Errors are no longer logged to the console, as it can conflict with an consumer-side catching and logging. Consumers can replicate the old behaviour by logging from [Apollo's `formatError` callback](https://www.apollographql.com/docs/apollo-server/data/errors/#for-the-client-response).
- Improved error message when unable to parse an input field

## 3.1.0 (2020-09-11)

### Changes
- Added `@excludeFromInput` directive to fields, omitting them from their parent's `___Params` input type.
- Entries in emitted events now define `id` for Insert operations if the inserted data has an `id`.

### Internal
- Restructured exports from directive modules

## 3.0.0 (2020-09-02)

### Changes
- Significantly expanded the hooks available to scripts.

### Fixes
- Filtering was broken, notably when using AND/OR.
- Development: `deploy-local` wasn't removing peer dependencies when run with `npm`.

### Breaking
- AutoCRUD's `typeDefs` export is now an array, matching documentation's examples. It was previously exported as `{ autocrud, relation }`.
- `precommitHooks` has been removed and replaced by the `preCommit` hook.
- `checkAuthorization` has been removed and replaced by the `preOperation` hook.

## 2.1.0 (2020-08-20)

### Changes
- Emitted PubSub events include the current and previous state for the base.
- Errors from relation queries are more explicit/helpful.

### Fixes
- Crashes when relating to a singular type:
  ```
  type Post {
    id: ID!
    content: PostContent @relation(to: "postID")
  }
  ```
- DataLoaders are cleared after mutations, prior to generating events, to avoid stale data.
- DataLoaders now use a transaction builder when loading data during a transaction, avoiding issues with SQLite locking the entire database.
- Avoids trying to create `orderBy` enums if there are no orderable fields.

### Internal
- Switched the project over to Yarn.
- Switched all tests using `mock-knex` over to a real database, and removed all `mock-knex` functionality.

## 2.0.0 (2020-07-31)

### Internal
- Improved build time.

### Breaking
- Bumped the minimum supported Node version up to 10.

## 1.2.2 (2020-07-28)

### Fixes
- The package is now rebuilt before versioning and publishing to NPM.

## 1.2.1 (2020-07-24)

Just a dependency update.

## 1.2.0 (2020-07-24)

### Changes
- GraphQL interface types are now supported.
- Union types are _not_ support, but they are explicitly skipped when generating AutoCRUD's various inputs to avoid crashing.
- Added CI testing.

## 1.1.0 (2020-07-10)

### Changes
- Emitted PubSub events now include the schema type of base models and affected items.

## 1.0.0 (2020-07-09)

### Changes
- `all____` queries now have an `orderBy` argument covering all of their fields (omitting IDs and unions) and fields on their relations.
- Restructured and standardized the emitted PubSub events to include more data and a unique ID. `getAutoCRUDContext` now accepts a `pubsubMetadata` option which will be passed straight through to emitted events as `metadata`.
- Removed `knex-on-duplicate-update` to allow broader database support.

### Fixes
- `Query` and `Mutation` types are generated if they don't exist in the user's schema.

### Internal
- Development: Added scripts to make local development and linking easier.

## 0.0.1 (2020-06-19)
- First public release
