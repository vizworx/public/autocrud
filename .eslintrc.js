module.exports = {
  extends: '@vizworx/eslint-config',
  env: {
    jest: true,
  },
  overrides: [
    {
      files: [
        '**/*spec.js',
        'src/testUtils/*.js',
        '**/testing/**',
      ],
      rules: {
        'import/no-extraneous-dependencies': [
          'error',
          { devDependencies: true },
        ],
      },
    },
  ],
  rules: {
    'padding-line-between-statements': [
      'error',
      {
        blankLine: 'always',
        prev: [
          'block',
          'block-like',
          'cjs-export',
          'class',
          'export',
          'import',
          'multiline-expression',
          'multiline-const',
          'multiline-let',
        ],
        next: '*',
      },
      {
        blankLine: 'any',
        prev: ['export', 'import'],
        next: ['export', 'import'],
      },
      {
        blankLine: 'always',
        prev: '*',
        next: 'if',
      },
      {
        blankLine: 'always',
        prev: 'const',
        next: 'return',
      },
    ],
    'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
  },
};
