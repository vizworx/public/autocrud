# AutoCRUD - Directives

<!-- AUTO-GENERATED-CONTENT:START (TOC) -->
- [`@autocrud` (on a type)](#autocrud-on-a-type)
  - [Arguments](#arguments)
  - [Pluralization](#pluralization)
  - [Example](#example)
  - [Interfaces](#interfaces)
    - [Type Resolvers](#type-resolvers)
- [`@relation` (on a field)](#relation-on-a-field)
  - [Relationship types](#relationship-types)
    - [Direct](#direct)
    - [Indirect](#indirect)
    - [Explicitly Typed](#explicitly-typed)
- [`@excludeFromInput` (on a field)](#excludefrominput-on-a-field)
- [`@dbColumn` (on a field)](#dbcolumn-on-a-field)
<!-- AUTO-GENERATED-CONTENT:END -->

## `@autocrud` (on a type)
This directive generates queries, mutations, and input arguments for a given type and database table.

### Arguments

| Arg | Type | Default | Description |
|-----|------|---------|-------------|
| table | String | **required** | A database table matching this type
| pivotInput | String | none | Used by the `@relation` directive; see [Explicitly Typed](#explicitly-typed)
| readAll | Boolean | true | Generate an `all` query for this type
| readOne | Boolean | true | Generate a single query (by ID) for this type
| create | Boolean | true | Generate a `create` mutation for this type
| update | Boolean | true | Generate an `update` mutation for this type
| delete | Boolean | true | Generate a `delete` mutation for this type

### Pluralization
AutoCRUD does **not** use any contextual rules when pluralizing types (i.e. `Person` -> `People`). Types ending with `s` are left as their singular name, and types ending with `y` use `ies` (`Story` -> `Stories`).

### Example
Given:

```graphql
type User @autocrud(table: "users") {
  id: ID!
  createdAt: DateTime!
  updatedAt: DateTime!
  email: String!
  name: String!
  avatarURL: String!
}
```

The resulting schema will contain:

```graphql
type Query {
  allUsers(filter: UserFilter, orderBy: [UserOrderBy], limit: Int, offset: Int): [User!]!
  user(id: ID!): User
}

type Mutation {
  createUser(user: UserParams!): User
  updateUser(id: ID!, user: UserParams!): User
  deleteUser(id: ID!): Boolean
}

input UserFilter {
  createdAt: DateTime
  createdAt_lt: DateTime
  createdAt_lte: DateTime
  createdAt_gt: DateTime
  createdAt_gte: DateTime
  createdAt_in: [DateTime!]

  updatedAt: DateTime
  updatedAt_lt: DateTime
  updatedAt_lte: DateTime
  updatedAt_gt: DateTime
  updatedAt_gte: DateTime
  updatedAt_in: [DateTime!]

  email: String
  email_contains: String
  email_startsWith: String
  email_endsWith: String
  email_in: [String!]

  name: String
  name_contains: String
  name_startsWith: String
  name_endsWith: String
  name_in: [String!]

  avatarURL: String
  avatarURL_contains: String
  avatarURL_startsWith: String
  avatarURL_endsWith: String
  avatarURL_in: [String!]
}

input UserOrderBy {
  field: UserOrderByFields
  desc: Boolean
}

enum UserOrderByFields {
  createdAt
  updatedAt
  email
  name
  avatarURL
}
```

### Interfaces
Adding the `@autocrud` directive to an interface, and a corresponding type resolver, will provide filtering and ordering using the common fields of an interface's associated types, and will also generate a matching `all` query for the interface.

```js
const typeDefs = gql`
  interface Pet @autocrud {
    id: ID!
    name: String!
    ownerID: ID
    owner: Person @relation(from: "ownerID")
  }

  type Cat implements Pet @autocrud(
    table: "cats",
  ) {
    id: ID!
    name: String!
    ownerID: ID
    owner: Person @relation(from: "ownerID")

    likesBeingPet: Boolean!
  }

  type Dog implements Pet @autocrud(
    table: "dogs",
  ) {
    id: ID!
    name: String!
    ownerID: ID
    owner: Person @relation(from: "ownerID")

    favoriteToy: String!
  }

  type Person @autocrud(table: "people") {
    id: ID!
    name: String!
    phone: String!
    email: String!
    age: Int!
    pets: [Pet!]! @relation(to: "ownerID")
  }
`;
```

#### Type Resolvers
GraphQL requires a type resolver on all interfaces and unions so the overall type-safety of a schema can be enforced. Because the schema is built prior to running AutoCRUD's directives, however, this should be done explicitly ahead of time.

The first argument passed to a type resolver is the database response. AutoCRUD includes the appropriate GraphQL model here, allowing the resolver in most cases to be quite simple:

```
const resolvers = {
  Pet: { __resolveType: (result) => result.autocrud_model },
};
```

##### Arguments

| Arg | Description |
|-----|-------------|
| result | A database row + `autocrud_model: Type` |
| contextValue | GraphQL context |
| info | GraphQL field |
| returnType | GraphQL type |

Interfaces can have relational fields, but with a few restrictions:
- The `@relation` directive **must** appear on both the interface and type declarations for a given field.
- Both must reference the same column (`from: "ownerID"`).
- `through` relations are not currently supported.
- Pivot relationships (explicitly typed or via a many-many table in the database) are not currently supported.
- When fetching relational fields in an ordered query, the related data is not ordered; AutoCRUD returns it exactly as it comes from the database. That is, given:
  ```graphql
    query {
    allPersons(orderBy: [
      { field: pets_name },
    ]) {
      name
      pets { name }
    }
  }
  ```
People will be ordered by their first alphabetical pet's name, but the pets themselves will not be.

## `@relation` (on a field)
This directive is used to establish links between models, and supports several different use cases.

*Note*: Relationships are queried using the target type's `all___` query, so for the moment any type being related to _must_ have an `all___` query enabled in its `@autocrud` config.

### Relationship types

#### Direct
- The table has a field containing the related item's ID
- Can be used for 1:1 or 1:M relationships
- The database column does _not_ have to be exposed as a field in the schema
- The `@relation` field cannot have the same name as the database column it references.
```graphql
type Tag @autocrud(table: "tags") {
  id: ID!
  childOfID: ID
  childOf: Tag @relation(from: "childOfID") # Can only be a child of one parent
  parentOf: [Tag!]! @relation(to: "childOfID") # Parents can have many children
}
```
Use `from` to reference a field on the current type, and `to` to reference a field on the related type. If the `from` or `to` fields use a [`@dbColumn`](#dbcolumn-on-a-field) directive, provide the GraphQL field name as the value.

#### Indirect
- The database has a simple pivot table defining the relationship, i.e.
  ```
  assets_vignettes: [
    { assetID: 1, vignetteID: 2 }
  ]
  ```
- Can be used for 1:1, 1:M, or M:M relationships
```graphql
type Book @autocrud(table: "books") {
  id: ID!
  authors: [Author!]! @relation(through: {
    table: "authors_books",
    near: "bookID",
    far: authorID"
  })
}

type Author @autocrud(table: "authors") {
  id: ID!
  books: [Book!]! @relation(through: {
    table: "authors_books",
    near: "authorID",
    far: "bookID"
  })
}
```
`near` refers to the pivot field associated with the current type, while `far` refers to the pivot field associated with the related type.

#### Explicitly Typed
- The relationship is exposed as its own type, e.g. if the relationship includes
metadata such as a weighting.
- The database has a pivot table defining the relationship and metadata
```graphql
input WeightedID {
  id: ID!
  weight: Int!
}

type VignetteTag @autocrud(
  table: "vignettes_tags",
  pivotInput: "WeightedID",
) {
  vignette: Vignette! @relation(from: "vignetteID")
  vignetteID: ID!
  tag: Tag! @relation(from: "tagID")
  tagID: ID!
  weight: Int!
}

type Tag @autocrud(table: "tags") {
  id: ID!
  vignettes: [VignetteTag!]! @relation(to: "tagID")
}

type Vignette @autocrud(table: "vignettes") {
  id: ID!
  name: String!
  tags: [VignetteTag!]! @relation(to: "vignetteID")
}
```
The `pivotInput` argument is used when generating input fields for the relations. From the above schema, AutoCRUD would create:
```graphql
input VignetteParams {
  name: String!
  tagIDs: [WeightedID!]!
}
```
This allows entries in the pivot table to be created and updated by mutations on either side.

## `@excludeFromInput` (on a field)
This directive causes a field to be omitted from its parent's input type.

For example:
```graphql
type Book @autocrud(table: "books") {
  id: ID!
  title: String!
  lastSearched: DateTime @excludeFromInput
}
```
The book's last-searched date isn't known or required by a user when mutating it, and thus we don't want it to be required by `BookParams`.

## `@dbColumn` (on a field)
This directive allows you to map between a GraphQL field and a DB column with a different name. You cannot use this directive on the `id` field, which is required by AutoCRUD for processing any mutations or relationships.

For example:
```graphql
type Book @autocrud(table: "books") {
  id: ID!
  title: String!
  urlSlug: String! @dbColumn(name: "slug")
}
```
GraphQL will expose the `urlSlug` field, but any DB queries will use the `slug` column
