# AutoCRUD - Event Reporting

<!-- AUTO-GENERATED-CONTENT:START (TOC) -->

<!-- AUTO-GENERATED-CONTENT:END -->

This example uses `apollo-server-express`, but Autocrud should be expected to work with any library following the GraphQL spec.

If an instance of [PubSub](https://www.apollographql.com/docs/apollo-server/data/subscriptions/) is passed to `getAutoCRUDContext`, three
events are emitted following successful mutations: `AUTOCRUD.CREATE`, `AUTOCRUD.UPDATE`, and `AUTOCRUD.DELETE`.

```js
import { ApolloServer, PubSub } from 'apollo-server-express';
import knex from './setupKnexInstance';

//
// Create a schema, as above
//

const pubsub = new PubSub();

const handleAutoCRUDChange = ({ changes }) => {
  console.log('AutoCRUD performed a mutation:');
  console.log(JSON.stringify(changes, null, '  '));
};

pubsub.subscribe('AUTOCRUD.CREATE', handleAutoCRUDChange);
pubsub.subscribe('AUTOCRUD.UPDATE', handleAutoCRUDChange);
pubsub.subscribe('AUTOCRUD.DELETE', handleAutoCRUDChange);

const { getContext: getAutoCRUDContext } = AutoCRUD.initialize(schema);

return new ApolloServer({
  schema,
  context: () => ({
    ...getAutoCRUDContext({
      knex,
      pubsub,
      pubsubMetadata
    }),
  }),
});
```

All events follow the same format:
```
{
  "changes": [
    {
      "operation": "delete",
      "table": "assets",
      "type": "Asset",
      "id": "b5354033-11a3-478e-8eea-c16f2d4d7149",
      "values": {
        "id": "b5354033-11a3-478e-8eea-c16f2d4d7149",
        "createdAt": "2020-08-13T22:02:21.000Z",
        "updatedAt": "2020-08-13T22:02:21.000Z",
        "key": "A123456",
        "name": "Les Paul",
        "assetDetails": "",
        "file": "user/assets/b5354033-11a3-478e-8eea-c16f2d4d7149/3e524ab7-be52-4381-ba0d-26aacf3fa181",
        "startYear": null,
        "endYear": null,
        "yearPublished": null,
        "fileType": "image"
      }
    }
  ],
  "mutation": {
    "name": "deleteAsset",                              // GraphQL mutation
    "type": "Asset",                                    // GraphQL type
    "table": "assets",                                  // Database table
    "targetID": "b5354033-11a3-478e-8eea-c16f2d4d7149"  // ID of the base item (for update and delete)
  },
  "currentState": null,
  "prevState": {
    "id": "b5354033-11a3-478e-8eea-c16f2d4d7149",
    "createdAt": "2020-08-13T22:02:21.000Z",
    "updatedAt": "2020-08-13T22:02:21.000Z",
    "key": "A123456",
    "name": "Les Paul",
    "assetDetails": "",
    "file": "user/assets/b5354033-11a3-478e-8eea-c16f2d4d7149/3e524ab7-be52-4381-ba0d-26aacf3fa181",
    "startYear": null,
    "endYear": null,
    "yearPublished": null,
    "fileType": "image"
  },
  // Unique identifier for each event
  "changeUUID": "eb0c8c22-8c28-41fa-94e5-5e78021beb29",
  // pubsubMetadata provided to AutoCRUD's context
  "metadata": {
    "auth": {
      "asyncAuth": {},
      "asyncUser": {},
      "asyncUserID": {}
    }
  }
}
```
