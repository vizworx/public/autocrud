module.exports = {
  presets: [[
    require.resolve('@babel/preset-env'),
    {
      useBuiltIns: 'usage',
      corejs: 3,
      targets: { node: '10' },
    },
  ]],
  plugins: [
    require.resolve('@babel/plugin-proposal-class-properties'),
  ],
};
